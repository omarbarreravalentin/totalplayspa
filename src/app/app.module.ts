import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing-module';
import { AppComponent } from './app.component';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Services
import { BioService } from './shared/services/bio.service';
import { CsrfService } from './shared/services/csrf.service';
import { SpinnerService } from './shared/services/spinner.service';
import { OnLineService } from './shared/services/online.service';
import { StatusService } from './shared/services/status.service';
import { DatosService } from './shared/services/datos.service';
import { RedirectService } from './shared/services/redirect.service';
import { IntentosService } from './shared/services/intentos.service';
import { ConfigService } from './shared/services/config.service';
import { ModalService } from './shared/services/modal.service';
import { CacheService } from './shared/services/cache.service';
import { CanActivateAuthGuard } from './shared/guard/can-activate-auth.guard';
import { InterceptorService } from './shared/services/interceptor.service';
import { ModalComponent } from './shared/modal/modal.components';
import { LoadingComponent } from './shared/loading/loading.components';
import { StepsService } from './shared/services/steps.service';
import { DebugService } from './shared/services/debug.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

export function initConfigService(configService: ConfigService): Function {
  if (window.location.hostname === 'localhost') {
    const url = 'https://bioonboarding-web-mx-secmgmt-pmi-authesrv-dev.appls.cto1.paas.gsnetcloud.corp/';
    return () => configService.getConfig(url);
  } else {
    return () => configService.getConfig();
  }
}

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpClientModule,
    DeviceDetectorModule.forRoot(),
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      maxOpened: 8,
      timeOut: 2000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
  ],
  declarations: [
    AppComponent,
    ModalComponent,
    LoadingComponent
  ],
  entryComponents: [ModalComponent, LoadingComponent],
  providers: [
    DebugService,
    BioService,
    CsrfService,
    DatosService,
    StepsService,
    SpinnerService,
    OnLineService,
    IntentosService,
    StatusService,
    RedirectService,
    ConfigService,
    CanActivateAuthGuard,
    ModalService,
    InterceptorService,
    {
      provide: APP_INITIALIZER,
      useFactory: initConfigService,
      deps: [ConfigService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  bootstrap: [
    AppComponent
  ]
})
/**
 * Clase module inicializadora del boostrap aplicativo
 */
export class AppModule { }
