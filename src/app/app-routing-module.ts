import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '' },
  { path: 'bio', loadChildren: ()=>import ('./bio/bio.module').then(m=>m.BioModule) },

];

  //{ path: 'bio', loadChildren: './bio/bio.module#BioModule' },

//{ path: 'auth', loadChildren: 'src/app/auth/auth.module#AuthModule' }

//{ path: 'auth', loadChildren: () => import('src/app/auth/auth.module').then(m => m.AuthModule) }

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
/**
 * Clase module inicializadora del app routing
 */
export class AppRoutingModule { }