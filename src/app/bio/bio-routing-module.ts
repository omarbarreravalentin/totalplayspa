import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';
import { AvisoInicioComponent } from './components/aviso-inicio/aviso-inicio.component';
// import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AvisoCapturaDocumentosComponent } from './components/aviso-captura-documentos/aviso-captura-documentos.component';
import { CapturaDocumentosComponent } from './components/captura-documentos/captura-documentos.component';
import { AvisoCapturaFotoComponent } from './components/aviso-captura-foto/aviso-captura-foto.components';
import { CapturaFotoComponent } from './components/captura-foto/captura-foto.component';
import { AvisoCapturaVideoComponent } from './components/aviso-captura-video/aviso-captura-video.component';
import { AvisoShowOCR } from './components/aviso-show-ocr/aviso-show-ocr.component';

import { CapturaVideoComponent } from './components/captura-video/captura-video.component';
import { AvisoFinalComponent } from './components/aviso-final/aviso-final.component';
// Errors
import { AvisoTimeOutComponent } from './components/aviso-time-out/aviso-time-out.component';
import { AvisoOffLineComponent } from './components/aviso-off-line/aviso-off-line.component';
import { AvisoPermisosComponent } from './components/aviso-permisos/aviso-permisos.component';
import { AvisoIncompatibleBrowserComponent } from './components/aviso-incompatible-browser/aviso-incompatible-browser.component';
import { CanActivateAuthGuard } from '../shared/guard/can-activate-auth.guard';
import { AvisoIntentosMaximoComponent } from './components/aviso-intentos-maximo/aviso-intentos-maximo.component';
/**
 * constante para almacenar rutas del modulo Bio
 */
const routes: Routes = [
  { path: '', component: AvisoInicioComponent },
  { path: 'infoDocuments', component: AvisoCapturaDocumentosComponent, canActivate: [CanActivateAuthGuard] },
  { path: 'captureDocuments', component: CapturaDocumentosComponent, canActivate: [CanActivateAuthGuard] },
  { path: 'infoFoto', component: AvisoCapturaFotoComponent, canActivate: [CanActivateAuthGuard] },
  { path: 'captureFoto', component: CapturaFotoComponent, canActivate: [CanActivateAuthGuard] },
  { path: 'infoVideo', component: AvisoCapturaVideoComponent, canActivate: [CanActivateAuthGuard] },
  { path: 'captureVideo', component: CapturaVideoComponent, canActivate: [CanActivateAuthGuard] },
  { path: 'timeOut', component: AvisoTimeOutComponent },
  { path: 'showOCR', component: AvisoShowOCR },
  { path: 'offLine', component: AvisoOffLineComponent },
  { path: 'intentos', component: AvisoIntentosMaximoComponent },
  { path: 'permisos', component: AvisoPermisosComponent },
  { path: 'incompatible', component: AvisoIncompatibleBrowserComponent },
  { path: 'final', component: AvisoFinalComponent, canActivate: [CanActivateAuthGuard] },
  { path: '', redirectTo: '', pathMatch: 'full' },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
/**
 * Clase para inicializar modulo lazy loading BioRouting
 */
export class BioRoutingModule { }
