import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BioRoutingModule } from './bio-routing-module';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AvisoInicioComponent } from './components/aviso-inicio/aviso-inicio.component';
// import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AvisoCapturaDocumentosComponent } from './components/aviso-captura-documentos/aviso-captura-documentos.component';
import { CapturaDocumentosComponent } from './components/captura-documentos/captura-documentos.component';
import { AvisoCapturaFotoComponent } from './components/aviso-captura-foto/aviso-captura-foto.components';
import { CapturaFotoComponent } from './components/captura-foto/captura-foto.component';
import { AvisoCapturaVideoComponent } from './components/aviso-captura-video/aviso-captura-video.component';
import { AvisoShowOCR } from './components/aviso-show-ocr/aviso-show-ocr.component';

import { CapturaVideoComponent } from './components/captura-video/captura-video.component';
import { AvisoFinalComponent } from './components/aviso-final/aviso-final.component';
// Errors
import { AvisoTimeOutComponent } from './components/aviso-time-out/aviso-time-out.component';
import { AvisoOffLineComponent } from './components/aviso-off-line/aviso-off-line.component';
import { AvisoPermisosComponent } from './components/aviso-permisos/aviso-permisos.component';
import { AvisoIncompatibleBrowserComponent } from './components/aviso-incompatible-browser/aviso-incompatible-browser.component';
import { AvisoIntentosMaximoComponent } from './components/aviso-intentos-maximo/aviso-intentos-maximo.component';

// Services
import { OnLineService } from '../shared/services/online.service';
import { StatusService } from '../shared/services/status.service';
import { DatosService } from '../shared/services/datos.service';

import { RedirectService } from '../shared/services/redirect.service';
import { IntentosService } from '../shared/services/intentos.service';
import { CacheService } from '../shared/services/cache.service';

import { CanActivateAuthGuard } from '../shared/guard/can-activate-auth.guard';
import { CapturaDocumentoBioService } from '../shared/services/captura-documento.bio.service';
import { CapturaFotoBioService } from '../shared/services/captura-foto.bio.service';
import { CapturaVideoBioService } from '../shared/services/captura-video.bio.service';

@NgModule({
  imports: [
    CommonModule,
    DeviceDetectorModule.forRoot(),
    BioRoutingModule,
    NgbModule
  ],
  declarations: [
    // PageNotFoundComponent,
    AvisoInicioComponent,
    AvisoCapturaDocumentosComponent,
    CapturaDocumentosComponent,
    AvisoCapturaFotoComponent,
    CapturaFotoComponent,
    AvisoCapturaVideoComponent,
    AvisoShowOCR,
    CapturaVideoComponent,
    AvisoFinalComponent,
    AvisoTimeOutComponent,
    AvisoOffLineComponent,
    AvisoPermisosComponent,
    AvisoIncompatibleBrowserComponent,
    AvisoIntentosMaximoComponent
  ],
  providers: [
    OnLineService,
    IntentosService,
    StatusService,
    DatosService,
    RedirectService,
    CanActivateAuthGuard,
    CapturaDocumentoBioService,
    CapturaFotoBioService,
    CacheService,
    CapturaVideoBioService,
  ]
})
/**
 * Clase para inicializar modulo lazy loading Bio
 */
export class BioModule { }
