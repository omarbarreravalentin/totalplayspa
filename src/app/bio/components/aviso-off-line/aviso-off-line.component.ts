import { Component} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RedirectService } from '../../../shared/services/redirect.service';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { CacheService } from '../../../shared/services/cache.service';

@Component({
  selector: 'saol-aviso-off-line',
  templateUrl: 'aviso-off-line.component.html'
})
/**
 * Clase para mostrar el aviso de sin conexion a la red
 */
export class AvisoOffLineComponent {
/**
   * Variable para  guardar base64 del icono santander
   */
  public inicio:any;

  /**
   * Variable para  guardar base64 del icono offline
   */
  public offline:any;

  /**
   * Inicializa servicio  RedirectService
   */
  constructor(private redirectService: RedirectService,
    private cacheService:CacheService,
    private domSanitizer: DomSanitizer,
    private spinnerService: SpinnerService) {
    this.spinnerService.sendSpinner({ status: false });
    this.cacheService.getCache().subscribe(paso => {
      if(paso){
        this.inicio = domSanitizer.bypassSecurityTrustResourceUrl(paso.inicio);
        this.offline = domSanitizer.bypassSecurityTrustUrl(paso.offline);
      }
    });
  }

  /**
   * Envio a servicio peticion http 505 para regresra al comienzo
   */
  terminar() {
    this.redirectService.sendStatus({ code: 505 });
  }

  /**
   * Envio a servicio peticion http 505 para regresra al comienzo
   */
  refresh(){
    location.reload();
  }
}