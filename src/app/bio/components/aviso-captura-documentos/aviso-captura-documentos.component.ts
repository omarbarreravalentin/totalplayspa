import { Component, OnInit } from '@angular/core';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { StatusService } from '../../../shared/services/status.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalService } from '../../../shared/services/modal.service';
import { StepsService } from '../../../shared/services/steps.service';
import { ConfigService } from '../../../shared/services/config.service';

@Component({
  selector: 'sacd-aviso-captura-documentos',
  templateUrl: 'aviso-captura-documentos.component.html'
})
/** 
 * Clase que muestra aviso para capturar los documentos del usuario
 */
export class AvisoCapturaDocumentosComponent implements OnInit {
  /**
   * Variable para guardar status boton cancelar
   */
  statusBtn: boolean;
  /**
   * Inicializa servicios para stus, router, modal
   */
  constructor(
    private step: StepsService,
    private statusService: StatusService,
    private route: Router,
    private spinnerService: SpinnerService,
    private router: ActivatedRoute,
    private configService: ConfigService,
    private modalService: ModalService) {
    this.statusBtn = true;
    this.spinnerService.sendSpinner({ status: false });
  }
  /**
   * Clase para mostrar aviso de incompatibilidad del dispositivo
   */
  ngOnInit(): void {
    this.statusService.sendStatus(this.router.url);
  }
  /**
   * Metodo para enviar parametros al modal y abrirlo para mandar mensaje personalizado
   */
  openModal() {
    this.modalService.sendModal({
      open: true,
      modalHeader: this.configService.config.public.modal_CancelaEnrolamiento_header,
      modalBody: this.configService.config.public.modal_CancelaEnrolamiento_body,
      btnCancelaProceso: true,
      btnCierraModal: true,
      btnEnterado: false,
      txtCancelaProceso: this.configService.config.public.modal_CancelaEnrolamiento_aceptar,
      txtCierraModal: this.configService.config.public.modal_CancelaEnrolamiento_cierra
    });
  }
  /**
   * Metodo para redirigir a inicio aplicativo
   */
  captureDocuments() {
    this.configService.getFullscreen(document.documentElement, "landscape");
    this.step.sendStep("captureDocs");
    this.route.navigate(['/bio/captureDocuments'], { queryParams: { date: new Date().getTime() } });
  }
}
