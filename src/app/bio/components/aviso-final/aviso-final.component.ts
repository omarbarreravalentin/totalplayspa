import { Compiler, Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'sf-aviso-final',
  templateUrl: 'aviso-final.component.html'
})
/**
 * Clase para mostrar aviso final del aplicativo
 */
export class AvisoFinalComponent implements OnDestroy {
  /**
   * Inicializa servicio  RedirectService
   */
  constructor(private router: Router, private compiler: Compiler) { }
  /**
   * Envio a servicio peticion http 505 para regresra al comienzo
   */
  terminar() {
    localStorage.clear();
    this.router.navigate(['/']);
  }
  /**
   * Metodo para capturar unmount del componente, al destruirse recarga y limpia cache
   */
  ngOnDestroy(): void {
    window.location.reload(true);
    this.compiler.clearCache();
  }
}
