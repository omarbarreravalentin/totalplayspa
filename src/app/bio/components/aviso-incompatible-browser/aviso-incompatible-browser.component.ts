import { Component } from '@angular/core';
import { RedirectService } from '../../../shared/services/redirect.service';
import { SpinnerService } from '../../../shared/services/spinner.service';

@Component({
  selector: 'saib-aviso-incompatible-browser',
  templateUrl: 'aviso-incompatible-browser.component.html'
})
/**
 * Clase para mostrar aviso de incompatibilidad del dispositivo
 */
export class AvisoIncompatibleBrowserComponent {
  /**
   * Inicializa servicio  RedirectService
   */
  constructor(private redirectService: RedirectService,
    private spinnerService: SpinnerService) {
    // DESACTIVA EL BTN RETROCEDER
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });
    
    this.spinnerService.sendSpinner({ status: false });
  }
  /**
   * Envio a servicio peticion http 505 para regresra al comienzo
   */
  terminar() {
    this.redirectService.sendStatus({ code: 505 });
  }
}
