import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { StatusService } from '../../../shared/services/status.service';
import { StepsService } from '../../../shared/services/steps.service';
import { ConfigService } from '../../../shared/services/config.service';
import { DebugService } from '../../../shared/services/debug.service';
import { CacheService } from '../../../shared/services/cache.service';
import { Subscription } from 'rxjs';

/*Constante de tipo window.navigator*/
declare const navigator: any;

/*Component de la clase*/
@Component({
  selector: 'sai-aviso-inicio',
  templateUrl: 'aviso-inicio.component.html'
})
/**
 * Clase que muestra aviso de inicio del aplicativo, asi mismo valida token opaco
 */
export class AvisoInicioComponent implements OnInit, OnDestroy {
  /**
   * Variable para validar si es un navegador compatible
   */
  deviceInfo = null;
  /**
 * Variable para susbcribirse a servicio del paso actual
 */
  public subscriptionStep$: Subscription;

  /**
   * Inicializa servicios para deteccion de disposistivos, router , status ....
   */
  constructor(
    private deviceService: DeviceDetectorService,
    public route: Router,
    private statusSpinner: SpinnerService,
    private statusService: StatusService,
    private configService: ConfigService,
    private cacheService: CacheService,
    private step: StepsService,
    private debugService: DebugService) {
    window.addEventListener('beforeunload', (event) => {
      let result = null;
      this.step.getStep().subscribe(paso => {
        if (paso) {
          switch (paso) {
            case "init":
            case "captureFoto":
            case "captureDocs":
            case "captureVideo":
            case "infoFoto":
              result = '¿Desea abandonar el sitio?';
              event.returnValue = result;
              break;
          }
        }
      });
    });
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });
    this.subscriptionStep$ = this.step.getStep().subscribe(paso => {
      if (paso) {
        if (paso === "tokenOK") {
          switch (paso) {
            case "captureFoto":
              this.route.navigate(['/bio/captureFoto'], { skipLocationChange: true });
              break;
            case "captureVideo":
              this.route.navigate(['/bio/captureVideo'], { skipLocationChange: true });
              break;
            case "infoFoto":
              this.route.navigate(['/bio/infoFoto'], { skipLocationChange: true });
              break;
            case "captureDocs":
              this.route.navigate(['/bio/captureDocuments']);
              break
          }
        } else {
          this.route.navigate(['/']);
        }
      } else {
        this.route.navigate(['/']);
      }
    });
    window.addEventListener("orientationchange",this.functionCheckOrientation.bind(this), false);
    this.epicFunction();
  }
  /**
   * Metodo que inicializa el componente llamando el servicio del status
   */
  ngOnInit(): void {
    let objCache = {};
    this.toDataURL('assets/img/santander/flamaroja.png', function (dataUrl: string) {
      objCache["inicio"] = dataUrl;
    });
    this.toDataURL('assets/img/santander/offline.svg', function (dataUrl: string) {
      objCache["offline"] = dataUrl;
    });
    setTimeout(() => {
      this.cacheService.sendCache(objCache);
    }, 1000);
    this.statusService.sendStatus(this.route.url);
    this.statusSpinner.sendSpinner({ status: false });
  }
  /**
   * Metodo para validar token opaco y permitir acceso a la siguiente pantalla
   */
  captureDocs(): void {
    const infoScreen={
      screenWidth:screen.width,
      screenHeight:screen.height,
      screenavailWidth:screen.availWidth,
      screenavailHeight:screen.availHeight,
      screenOrientation:window.orientation
    }
  
    this.debugService.debug({
      console: true,
      alert: true,
      msjConsole: "INFO " + new Date() + " "+JSON.stringify(infoScreen),
      msjAlert: JSON.stringify(infoScreen)
    });
    
    const constraints = {
      video: {
        facingMode: (false ? "user" : "environment"),
        width: {
          min: 1024,
          ideal: 1920,
          max: 1920
        },
        height: {
          min: 720,
          ideal: 1080,
          max: 1080
        }
      },
      audio: true
    };

    if (navigator.mediaDevices === undefined) {
      navigator.mediaDevices = {};
    }

    if (navigator.mediaDevices.getUserMedia === undefined) {
      navigator.mediaDevices.getUserMedia = this.promisifiedOldGUM;
    }

    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
      this.configService.getFullscreen(document.documentElement, "landscape");
      this.step.sendStep("captureDocs");
      this.route.navigate(['/bio/captureDocuments']);
    }).catch((err) => {
      this.debugService.debug({
        console: true,
        alert: true,
        msjConsole: "ERR " + new Date() + " START_MEDIA_DEVICES :" + err.name,
        msjAlert: "START_MEDIA_DEVICES :" + err.name
      });
      this.configService.getFullscreen(document.documentElement, undefined);
      this.step.sendStep("captureDocs");
      this.route.navigate(['/bio/permisos']);
    });
  }
  /**
   * Metodo que valida el tipo de navegador, para detectar navegadores incompatibles con veridas
   */
  epicFunction(): void {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.deviceService.isMobile();
    this.deviceService.isTablet();
    this.deviceService.isDesktop();
    const browser = this.deviceInfo.browser;
    const os = this.deviceInfo.os;
    this.debugService.debug({
      console: true,
      alert: false,
      msjConsole: JSON.stringify(this.deviceInfo),
      msjAlert: ""
    });
    // if (this.deviceService.isMobile()) {
    //   window.scroll({
    //     top: 5,
    //     behavior: 'smooth'
    //   });
    // }
    if (browser === 'IE' || (os === 'iOS' && browser === 'Chrome' && this.deviceService.isMobile())) {
      this.route.navigate(['/bio/incompatible']);
    }
  }

  /**
 * Metodo para ejecutar unsubscribe del componente, y limpiar intervalos
 */
  ngOnDestroy() {
    try {
      this.subscriptionStep$.unsubscribe();
    } catch (error) { }
  }

  /**
      * Metodo que obtiene el base64 de una imagen
      * @param url url del fichero
      * @param callback promise de respuesta
      */
  toDataURL(url: any, callback: any) {
    let xhr = new XMLHttpRequest();
    xhr.onload = function () {
      let reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  /**
   * Método que valida UserMedia para antiguos browsers 
   * @param constraints parametros de permisos camara-microfono
   * @param successCallback funcion en caso de ser exitosa la asignacion
   * @param errorCallback error en caso de no poder asignar los permisos
   * @returns  promise de los permisos camera
   */
  promisifiedOldGUM(constraints, successCallback, errorCallback) {
    this.debugService.debug({
      console: true,
      alert: true,
      msjConsole: "INFO " + new Date() + " USING_OLD_GUM",
      msjAlert: "USING_OLD_GUM"
    });
    // First get ahold of getUserMedia, if present
    let getUserMedia = (navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia);
    // Some browsers just don't implement it - return a rejected promise with an error
    // to keep a consistent interface
    if (!getUserMedia) {
      return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
    }
    // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
    return new Promise(function (successCallbackx, errorCallbackx) {
      getUserMedia.call(navigator, constraints, successCallback, errorCallback);
    });
  }

  /**
     * Metodo para validar la orientacion del dispositivo
     * @param fnOrientation - grados de la orientacion
     */
    functionCheckOrientation(fnOrientation:any) {
      const orientation=window.orientation;
      this.debugService.debug({
        console: true,
        alert: true,
        msjConsole: "INFO " + new Date() + " Orientation: "+orientation,
        msjAlert: orientation
      });

      if (orientation == 90 || orientation == -90) {
        this.debugService.debug({
          console: true,
          alert: true,
          msjConsole: "INFO " + new Date() + " LANDSCAPE",
          msjAlert: "LANDSCAPE"
        });
      } else if (orientation == 0 || orientation == 180) {
        this.debugService.debug({
          console: true,
          alert: true,
          msjConsole: "INFO " + new Date() + " PORTRAIT",
          msjAlert: "PORTRAIT"
        });
      }
    }
}
