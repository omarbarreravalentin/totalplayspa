import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { CapturaVideoBioService } from '../../../shared/services/captura-video.bio.service';
import { OnLineService } from '../../../shared/services/online.service';
import { ModalService } from '../../../shared/services/modal.service';
import { ConfigService } from '../../../shared/services/config.service';
import { RedirectService } from '../../../shared/services/redirect.service';
import { CsrfService } from '../../../shared/services/csrf.service';
import { Subscription } from 'rxjs';
import { StepsService } from '../../../shared/services/steps.service';
import { timeout } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import { DebugService } from '../../../shared/services/debug.service';
import { CapturaDocumentoBioService } from '../../../shared/services/captura-documento.bio.service';
import { DatosService } from '../../../shared/services/datos.service';
import { BioService } from '../../../shared/services/bio.service';

/**
 * Constante para hacer refrencia a la libreria veridas makeVDVideoWidget
 */
declare const makeVDVideoWidget: any;
/**
 * Constante para hacer refrencia a la libreria resumable
 */
declare const Resumable: any;
/**
 * Constante para almacenar numero de intentos
 */
let countTime: any;
@Component({
  selector: 'scv-captura-video',
  templateUrl: 'captura-video.component.html',
  styleUrls: ['captura-video.component.css']
})
/**
 * Clase para inicializar modulo captura de video bioonboarding
 */
export class CapturaVideoComponent implements OnInit, OnDestroy {

  /**
   * Variable Subscription para guardar observable csrf
   */
  public subscriptionCsrf$: Subscription;
  /**
   * Variable Subscription para guardar observable csrf
   */
  public subscriptionCsrfx$: Subscription;
  /**
   * Variable Subscription para guardar observable de la captura de video alternativo
   */
  public subscriptionAlternative$: Subscription;
  /**
   * Variable Subscription para guardar observable de la captura de video standard
   */
  public subscriptionStandard$: Subscription;

  /**
   * Variable para guardar intervalo de tiempo para los segundos
   */
  public intervalCapture: any;

  /**
   * Variable para  guardar el tiempo de captura
   */
  public counter: number;

  /**
   * Variable para  guardar tiempo pendiente de captura
   */
  public counterSeg: string;

  /**
   * Se inicializan instanciando los servicios a utilizar en el componente
   */
  constructor(
    private configService: ConfigService,
    private router: Router,
    private statusSpinner: SpinnerService,
    private onlineService: OnLineService,
    private modalService: ModalService,
    private redirectService: RedirectService,
    private capturaVideoService: CapturaVideoBioService,
    private csrfService: CsrfService,
    private deviceService: DeviceDetectorService,
    private debugService: DebugService,
    private capturaDocumentoService: CapturaDocumentoBioService,
    private bio:BioService,
    private datosService:DatosService,
    private step: StepsService) {
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });
    if (this.deviceService.isMobile()) {
      this.configService.getFullscreen(document.documentElement, "portrait");
    } else {
      this.configService.getFullscreen(document.documentElement, undefined);
    }
    this.statusSpinner.sendSpinner({ status: false });
    this.counter = 0;
    this.counterSeg = "";
  }
  /**
   * Metodo que inicializa los parametros para la libreria veridas makeVDVideoWidget, asi mismo se crean sus listener
   * dinamicos para cachar sus diferentes status de procesamiento
   */
  ngOnInit() {
    if (this.deviceService.isMobile()) {
      this.configService.getFullscreen(document.documentElement, "portrait");
    } else {
      this.configService.getFullscreen(document.documentElement, undefined);
    }
    countTime = 0;
    const tiempos = this.configService.config.public.video_recordingTimeStates;
    tiempos.forEach(num => {
      this.counter += num;
    });
    this.counter /= 1000;

    const VDVideo = makeVDVideoWidget();
    VDVideo({
      targetSelector: '#targetV',
      documents: [],
      alternativeRecordingTimeStates: this.configService.config.public.video_recordingTimeStates,
      recordingTimeStates: this.configService.config.public.video_recordingTimeStates,
      webrtcUnsupportedText: this.configService.config.public.video_webrtcUnsupportedText,
      displayErrors: this.configService.config.public.video_displayErrors,
      infoAlertShow: this.configService.config.public.video_infoAlertShow,
      infoAlertVideoSelfie: this.configService.config.public.video_infoAlertVideoSelfie,
      infoFaceRecording: this.configService.config.public.video_infoFaceRecording,
      infoObverseRecording: this.configService.config.public.video_infoObverseRecording,
      infoReverseRecording: this.configService.config.public.video_infoReverseRecording,
      continueText: this.configService.config.public.video_continueText,
      sdkBackgroundColorInactive: this.configService.config.public.video_sdkBackgroundColorInactive,
      borderColorCenteringAidInactive: this.configService.config.public.video_borderColorCenteringAidInactive,
      borderColorCenteringAidDefault: this.configService.config.public.video_borderColorCenteringAidDefault,
      detectionMessageBackgroundColor: this.configService.config.public.video_detectionMessageBackgroundColor,
      borderColorCenteringAidDetecting: this.configService.config.public.video_borderColorCenteringAidDetecting,
      detectionMessageTextColor: this.configService.config.public.video_detectionMessageTextColor,
      confirmationDialogBackgroundColor: this.configService.config.public.video_confirmationDialogBackgroundColor,
      confirmationDialogTextColor: this.configService.config.public.video_confirmationDialogTextColor,
      confirmationDialogLinkTextColor: this.configService.config.public.video_confirmationDialogLinkTextColor,
      setLandscapeDeviceMessage: this.configService.config.public.video_setLandscapeDeviceMessage,
      setPortraitDeviceMessage: this.configService.config.public.video_setPortraitDeviceMessage
    });
    const detectionEvents = [
      'VDVIDEO_mounted',
      'VDVideo_unmounted',
      'VDVideo_mountFailure',
      'VDVideo_cameraStarted',
      'VDVideo_cameraFailure',
      'VDVideo_standardVideoRecorderStart',
      'VDVideo_alternativeVideoRecorderStart',
      'VDVideo_standardVideoOutput',
      'VDVideo_alternativeVideoOutput',
      'VDVideo_detectionTimeout'
    ];

    detectionEvents.forEach((event) => {
      addEventListener(event, this.VideoDetectionEvent.bind(this), true);
    });
  }
  /**
   * Metodo para almacenar el video para sistema operativo ios
   * @param obj - video
   * @param operation - tipo de operacion a tratar
   */
  alternativeVideo(obj: Object, operation: string) {
    this.subscriptionCsrf$ = this.csrfService.getStatus().subscribe(datacsrf => {
      if (datacsrf && datacsrf.token) {
        this.redirectService.getStatus().subscribe(pathRedirect => {
          if (pathRedirect && pathRedirect.ruta) { 
            const codecsrf = datacsrf.token;
            const theBlob: any = new Blob([JSON.stringify(obj)], { type: 'video/webm' });
            theBlob.lastModifiedDate = new Date();
            theBlob.name = 'fileName';
    
            const fileraToUpload = new File([theBlob], 'name');
            let forma = new FormData();
            forma.append("file", fileraToUpload, fileraToUpload.name);
            forma.append('type', operation);
            this.statusSpinner.sendSpinner({ status: true, txtSpinner: 'Estamos trabajando...',subtxtSpinner:'Espera un poco.'});
            
            let idExpedientexxx=localStorage.getItem('idExpediente');
            let idSesionxxx=localStorage.getItem('idSesion');
            this.capturaDocumentoService.postCompleteDocument(operation,forma,idExpedientexxx,idSesionxxx)
              .pipe(timeout(this.configService.config.private.frontServices.document.timeOut))
              .subscribe(() => {
                this.router.navigate(['/bio/showOCR'], { skipLocationChange: true });
              }, response => {
                this.router.navigate(['/bio/showOCR'], { skipLocationChange: true });
              });  
          }});
      }
    });
  }
  /**
   * Metodo para detectar los diferentes ciclos de vida al procesar la captura del video
   * @param e
   */
  VideoDetectionEvent(e) {
    if (e && e.type) {
      const operation = e.type;
      this.debugService.debug({
        console: true,
        alert: true,
        msjConsole: "INFO " + new Date() + " VIDEO_OPERACION_REALIZADA " + operation,
        msjAlert: "VIDEO_OPERACION_REALIZADA " + operation
      });
      switch (operation) {
        case 'VDVideo_standardVideoRecorderStart':
        case 'VDVideo_alternativeVideoRecorderStart':
          this.configService.getFullscreen(document.documentElement, "portrait");
          this.intervalCapture = setInterval(() => {
            const tiempoRestante = --this.counter;
            if (tiempoRestante > 0) {
              this.counterSeg = "Te quedan "+tiempoRestante + " segundos";
            } else {
              this.counterSeg = "";
            }
          }, 1000);
          break;

        case 'VDVideo_cameraFailure':
          this.router.navigate(['/bio/permisos'], { skipLocationChange: true });
          break;

        case 'VDVideo_mountFailure':
          this.router.navigate(['/bio/incompatible'], { skipLocationChange: true });
          break;

        case 'VDVideo_standardVideoOutput':
          this.subscriptionCsrfx$ = this.csrfService.getStatus().subscribe(datacsrf => {
            if (datacsrf && datacsrf.token) {
              this.redirectService.getStatus().subscribe(pathRedirect => {
                if (pathRedirect && pathRedirect.ruta) {
                  const codecsrf = datacsrf.token;
                  const video = e.detail.recording.video;
                  video.lastModifiedDate = new Date();
                  video.name = 'file.webm';
                  const fileToUpload = <File>video;
    
                  let forma = new FormData();
                  forma.append("file", fileToUpload, fileToUpload.name);
                  forma.append('type', operation);
                  this.statusSpinner.sendSpinner({ status: true, txtSpinner: 'Estamos trabajando...',subtxtSpinner:'Espera un poco.'});
                  
                  let idExpedientexxx=localStorage.getItem('idExpediente');
                  let idSesionxxx=localStorage.getItem('idSesion');
                  this.capturaDocumentoService.postCompleteDocument(operation,forma,idExpedientexxx,idSesionxxx)
                    .pipe(timeout(this.configService.config.private.frontServices.document.timeOut))
                    .subscribe(() => {
                      this.router.navigate(['/bio/showOCR'], { skipLocationChange: true });
                    }, response => {
                      this.router.navigate(['/bio/showOCR'], { skipLocationChange: true });
                    });
                    
                  
                }});
            }
          });
          break;

        case 'VDVideo_alternativeVideoOutput':
          ++countTime;
          if (countTime === 1) {
            const obj = {
              audio: e.detail.recording.audio,
              video: e.detail.recording.video
            };
            this.alternativeVideo(obj, operation);
          }
          break;

        default:

          break;
      }
    }
  }
  /**
   * Metodo que envia subscripcion para abrir modal confirmacion
   */
  openModal(): void {
    this.modalService.sendModal({
      open: true,
      btnCancelaProceso: true,
      btnCierraModal: true,
      btnEnterado: false,
      modalHeader: this.configService.config.public.modal_CancelaEnrolamiento_header,
      modalBody: this.configService.config.public.modal_CancelaEnrolamiento_body,
      txtCancelaProceso: this.configService.config.public.modal_CancelaEnrolamiento_aceptar,
      txtCierraModal: this.configService.config.public.modal_CancelaEnrolamiento_cierra
    });
  }
  /**
   * Metodo para ejecutar unsubscribe del componente
   */
  ngOnDestroy() {
    try {
      clearInterval(this.intervalCapture);
      this.subscriptionCsrf$.unsubscribe();
      this.subscriptionAlternative$.unsubscribe();
      this.subscriptionStandard$.unsubscribe();
      this.subscriptionCsrfx$.unsubscribe();
    } catch (error) { }

  }
}
