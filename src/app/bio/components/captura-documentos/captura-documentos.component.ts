import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CapturaDocumentoBioService } from '../../../shared/services/captura-documento.bio.service';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { ModalService } from '../../../shared/services/modal.service';
import { IntentosService } from '../../../shared/services/intentos.service';
import { RedirectService } from '../../../shared/services/redirect.service';
import { ConfigService } from '../../../shared/services/config.service';
import { CsrfService } from '../../../shared/services/csrf.service';
import { timeout } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Subscription } from 'rxjs';
import { StepsService } from '../../../shared/services/steps.service';
import { DebugService } from '../../../shared/services/debug.service';
import { DatosService } from '../../../shared/services/datos.service';

/**
 * Constante para hacer refrencia a la libreria veridas makeVDSelfieWidget
 */
declare const makeVDDocumentWidget: any;
/**
 * Constante para hacer refrencia a la funcion loadWASM, para conocer si el .wasm ha sido cargado
 */
declare function loadWASM(): any;
/**
 * Constante para hacer refrencia a la libreria resumable
 */
declare const Resumable: any;
/**
 * Constante para guardar numero de intentos para guardar documento
 */
let papaRetries: number;
/**
 * Constante para conocer si el anverso ya ha sido enviado
 */
let anvProcesado: boolean;
/**
 * Constante para conocer si se ha ingresado al metodo de envio de anverso
 */
let cicloanv: boolean;
/**
 * Constante para conocer si el reverso ya ha sido enviado
 */
let revProcesado: boolean;
/**
 * Constante para conocer si se ha ingresado al metodo de envio de reverso
 */
let yaEnviado: boolean;
/**
   * Variable para obtener progreso de la captura de documento
  */
let progressAnvUploadDocs: number;

@Component({
  selector: 'scd-captura-documentos',
  templateUrl: 'captura-documentos.component.html'
})
/**
 * Clase para procesar la captura del documento bioonboarding, utiliza pluggin Resumable para dividir en partes la foto
 */
export class CapturaDocumentosComponent implements OnInit, OnDestroy {
  /**
   * Variable para validar status del boton siguiente
   */
  public showButton: boolean;
  /**
   * Variable para guardar intervalo de tiempo para la captura del documento
   */
  public intervalCapture: any;
  /**
   * Variable para guardar intervalo de tiempo para la captura del documento
   */
  public intervalWASM: any;
  /**
   * Variable para guardar intervalo de tiempo para conexion dispositivo
   */
  // public timeDevice: any;
  /**
   * Variable para susbcribirse a servicio de numero de intentos al capturar selfie
   */
  public subscriptionIntentos$: Subscription;
  /**
   * Variable Subscription para guardar observable csrf
   */
  public subscriptionCsrf$: Subscription;
  /**
     * Variable Subscription para guardar observable del documento
     */
  public subscriptionDoc$: Subscription;
  /**
     * Variable Subscription para guardar observable del proceso de enrolamiento
     */
  public subscriptionStep$: Subscription;
  public subscriptionDato$: Subscription;


  /**
   * Se inicializan los servicios que utilizara el componente, asi como asignacion de valores a avriables
   */
  constructor(
    private router: Router,
    private configService: ConfigService,
    private statusSpinner: SpinnerService,
    private intentosService: IntentosService,
    private modalService: ModalService,
    private deviceService: DeviceDetectorService,
    private redirectService: RedirectService,
    private capturaDocumentoService: CapturaDocumentoBioService,
    private csrfService: CsrfService,
    private debugService: DebugService,
    private datosService:DatosService,
    private step: StepsService) {
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });
    configService.getFullscreen(document.documentElement, "landscape");
    this.subscriptionStep$ = this.step.getStep().subscribe(paso => {
      if (paso && paso == "infoFoto") {
        this.configService.getFullscreen(document.documentElement, "portrait");
        this.router.navigate(['/bio/infoFoto'], { queryParams: { date: new Date().getTime() } });
      }
    });

    this.subscriptionDato$=this.datosService.getDatos().subscribe(paso => {
      if (paso) {
        this.debugService.debug({
          console: true,
          alert: true,
          msjConsole: "INFO " + new Date() + " DOC_RESPONSE CHIDO",
          msjAlert: "CHIDO 2 "
        });        }
    });

    this.showButton = true;
    papaRetries = 0;
    anvProcesado = false;
    revProcesado = false;
    yaEnviado = false;
    cicloanv = false;
    progressAnvUploadDocs = 0;
  }
  /**
   * Metodo para inicializar llamada a libreria makeVDDocumentWidget, asi mismo genera todos los listener donde se escucharan
   * los estados de desarrolo de la captura del documento
   */
  ngOnInit() {
    this.intervalWASM = setInterval(() => {
      const alfa = loadWASM();
      if (alfa) {
        clearInterval(this.intervalWASM);

        this.statusSpinner.sendSpinner({ status: false });
        this.subscribeIntentos();
        this.intervalCapture = setInterval(() => {
          const blurMsj: any = document.getElementsByClassName("vd-text-content");
          if (blurMsj && blurMsj[0] && blurMsj[0].innerHTML === this.configService.config.public.document_infoReviewBlurImageText) {
            const btnContinuar: any = document.getElementsByClassName('vd-button vd-continue');
            btnContinuar[0].hidden = true;
            this.configService.getFullscreen(document.documentElement, "landscape");
            const btnRepetir: any = document.getElementsByClassName('vd-button');
            btnRepetir[0].className = "vd-button vd-continuex";
          }

          const vdConfirmationx: any = document.getElementsByClassName('vd-button');
          if (vdConfirmationx && vdConfirmationx[0] && vdConfirmationx[0].innerHTML === this.configService.config.public.document_repeatText) {
            const vdConfirmationxs = vdConfirmationx[0];
            vdConfirmationxs.onclick = () => {
              this.configService.getFullscreen(document.documentElement, "landscape");
              this.intentosService.sendIntentos(true);
            };
          }
          const vdConfirmation: any = document.getElementsByClassName('vd-confirmation');
          if (vdConfirmation && vdConfirmation[0] && vdConfirmation[0].innerHTML) {
            this.showButton = false;
          } else {
            this.showButton = true;
          }
        }, 500);
        const VDDocument = makeVDDocumentWidget();
        VDDocument({
          targetSelector: '#target',
          documents: ['MX'],
          webrtcUnsupportedText: this.configService.config.public.document_webrtcUnsupportedText,
          displayErrors: this.configService.config.public.document_displayErrors,
          infoAlertShow: this.configService.config.public.document_infoAlertShow,
          infoAlertTwoSidedDocument: this.configService.config.public.document_infoAlertTwoSidedDocument,
          obverseNotFoundText: this.configService.config.public.document_obverseNotFoundText,
          reverseNotFoundText: this.configService.config.public.document_reverseNotFoundText,
          manualCaptureEnableDelay: this.configService.config.public.document_manualCaptureEnableDelay,
          manualCaptureText: this.configService.config.public.document_manualCaptureText,
          manualCaptureTextMobile: this.configService.config.public.document_manualCaptureTextMobile,
          infoReviewImageText: this.configService.config.public.document_infoReviewImageText,
          detectionTimeout: this.configService.config.public.document_detectionTimeout,
          reviewImage: this.configService.config.public.document_reviewImage,
          repeatText: this.configService.config.public.document_repeatText,
          mustRepeatText: this.configService.config.public.document_repeatText,
          infoReviewBlurImageText: this.configService.config.public.document_infoReviewBlurImageText,
          blurThresholdDesktop: this.configService.config.public.document_blurThresholdDesktop,
          blurThresholdMobile: this.configService.config.public.document_blurThresholdMobile,
          blurDetectorActive: this.configService.config.public.document_blurDetectorActive,
          continueText: this.configService.config.public.document_continueText,
          sdkBackgroundColorInactive: this.configService.config.public.document_sdkBackgroundColorInactive,
          borderColorCenteringAidInactive: this.configService.config.public.document_borderColorCenteringAidInactive,
          borderColorCenteringAidDefault: this.configService.config.public.document_borderColorCenteringAidDefault,
          borderColorCenteringAidDetecting: this.configService.config.public.document_borderColorCenteringAidDetecting,
          borderColorCenteringAidDetectingSuccess: this.configService.config.public.document_borderColorCenteringAidDetectingSuccess,
          outerGlowCenteringAidDefault: this.configService.config.public.document_outerGlowCenteringAidDefault,
          outerGlowCenteringAidDetecting: this.configService.config.public.document_outerGlowCenteringAidDetecting,
          detectionMessageBackgroundColor: this.configService.config.public.document_detectionMessageBackgroundColor,
          detectionMessageTextColor: this.configService.config.public.document_detectionMessageTextColor,
          confirmationDialogBackgroundColor: this.configService.config.public.document_confirmationDialogBackgroundColor,
          confirmationDialogTextColor: this.configService.config.public.document_confirmationDialogTextColor,
          confirmationDialogButtonTextColor: this.configService.config.public.document_confirmationDialogButtonTextColor,
          confirmationDialogLinkTextColor: this.configService.config.public.document_confirmationDialogLinkTextColor,
          confirmationDialogButtonBackgroundColor: this.configService.config.public.document_confirmationDialogButtonBackgroundColor,
          setLandscapeDeviceMessage: this.configService.config.public.document_setLandscapeDeviceMessage,
          setPortraitDeviceMessage: this.configService.config.public.document_setPortraitDeviceMessage,
          confirmationColorTick: this.configService.config.public.document_confirmationColorTick
        });

        const detectionEvents = [
          'VDDocument_mounted',
          'VDDocument_unmounted',
          'VDDocument_mountFailure',
          'VDDocument_cameraStarted',
          'VDDocument_cameraFailure',
          'VDDocument_obverseDetection',
          'VDDocument_reverseDetection',
          'VDDocument_detectionTimeout'
        ];
        detectionEvents.forEach((event) => {
          addEventListener(event, this.documentDetectionEvent.bind(this), true);
        });
      }
    }, 500);
  }

  /**
   * Metodo que proceso el reverso de la credencial
   * @param e Parametro del sdk
   * @param operation tipo de operación realizada por el sdk
   */
  reverseDetection(e: any, operation: any) {
    revProcesado = true;
    this.statusSpinner.sendSpinner({ status: true, txtSpinner: 'Estamos trabajando...',subtxtSpinner:'Espera un poco.'});
    this.subscriptionCsrf$ = this.csrfService.getStatus().subscribe(datacsrf => {
      if (datacsrf && datacsrf.token) {
        const codecsrf = datacsrf.token;
        const dataurl = e.detail;
        const arr = dataurl.split(',');
        const mime = arr[0].match(/:(.*?);/)[1];
        const bstr = atob(arr[1]);
        let n = bstr.length;
        const u8arr = new Uint8Array(n);
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
        }
        let interval = setInterval(() => {
          if (anvProcesado && !yaEnviado) {
            yaEnviado = true;
            clearInterval(interval);
            const fileToUpload = new File([u8arr], 'fileUpload.png', { type: mime });
            let forma=new FormData();
            forma.append("file", fileToUpload, fileToUpload.name);
            forma.append('type',operation);
            let idExpedientexxx=localStorage.getItem('idExpediente');
            let idSesionxxx=localStorage.getItem('idSesion');

            this.capturaDocumentoService.postCompleteDocument(operation,forma,idExpedientexxx,idSesionxxx)
            .pipe(timeout(this.configService.config.private.frontServices.document.timeOut))
            .subscribe(() => {
              let response={
                status:200
              };
              this.statusSpinner.sendSpinner({ status: true, txtSpinner: 'Estamos trabajando...',subtxtSpinner:'Espera un poco.'});
              response["status"]=200;
              this.debugService.debug({
                console: true,
                alert: true,
                msjConsole: "INFO " + new Date() + " DOC_RESPONSE " + JSON.stringify(response),
                msjAlert: "DOC_RESPONSE " + response.status
              });
              this.redirectService.sendStatus({ code: response.status, type: 'document' });
              if (response.status > 205) {
                this.modalService.sendModal({
                  open: true,
                  btnEnterado: true,
                  modalHeader: this.configService.config.public.modal_ErrorEnrolamiento_header,
                  modalBody: this.configService.config.public.modal_ErrorEnrolamiento_body,
                  txtEnterado: this.configService.config.public.modal_ErrorEnrolamiento_cierra,
                });
              }else {
                if (operation == 'VDDocument_reverseDetection') {
                  this.configService.getFullscreen(document.documentElement, "portrait");
                  this.step.sendStep("infoFoto");
                  this.router.navigate(['/bio/infoFoto'], { queryParams: { date: new Date().getTime() } });
                }
              }
            }, response => {
              response["status"]=500;
              this.statusSpinner.sendSpinner({ status: true, txtSpinner: 'Estamos trabajando...',subtxtSpinner:'Espera un poco.'});
              const code = response.status;
              this.debugService.debug({
                console: true,
                alert: true,
                msjConsole: "INFO " + new Date() + " DOC:RESPONSE " + JSON.stringify(response),
                msjAlert: "DOC:RESPONSE " + code
              });
              this.statusSpinner.sendSpinner({ status: false });
              this.redirectService.sendStatus({ code: code, type: 'document' });
              if (code > 205) {
                this.modalService.sendModal({
                  open: true,
                  btnEnterado: true,
                  modalHeader: this.configService.config.public.modal_ErrorEnrolamiento_header,
                  modalBody: this.configService.config.public.modal_ErrorEnrolamiento_body,
                  txtEnterado: this.configService.config.public.modal_ErrorEnrolamiento_cierra,
                });
              }
            });
          }
        }, 3000);
      } else {
        this.debugService.debug({
          console: true,
          alert: true,
          msjConsole: "ERR " + new Date() + " csrf_undefined",
          msjAlert: "csrf_undefined"
        });
      }
    });
  }

  /**
   * Metodo que proceso el frente de la credencial
   * @param e Parametro del sdk
   * @param operation tipo de operación realizada por el sdk
   */
  obverseDetection(e, operation) {
    if (!cicloanv) {
      cicloanv = true;
      this.subscriptionDato$=this.datosService.getDatos().subscribe(paso => {
        if (paso) {
          this.debugService.debug({
            console: true,
            alert: true,
            msjConsole: "INFO " + new Date() + " DOC_RESPONSE CHIDO",
            msjAlert: "CHIDO 1 "
          });       
         }
      });

      this.subscriptionStep$ = this.step.getStep().subscribe(paso => {
        if (paso && (paso == "timeoutReverso")) {
          anvProcesado = true;
        }
      });
      if (!anvProcesado) {
        this.subscriptionCsrf$ = this.csrfService.getStatus().subscribe(datacsrf => {
          if (datacsrf && datacsrf.token) {
            const codecsrf = datacsrf.token;
            const dataurl = e.detail;
            const arr = dataurl.split(',');
            const mime = arr[0].match(/:(.*?);/)[1];
            const bstr = atob(arr[1]);
            let n = bstr.length;
            const u8arr = new Uint8Array(n);
            while (n--) {
              u8arr[n] = bstr.charCodeAt(n);
            }
            const fileToUpload = new File([u8arr], 'fileUpload.png', { type: mime });
            let forma=new FormData();
            forma.append("file", fileToUpload, fileToUpload.name);
            forma.append('type',operation);

            let idExpedientexxx=localStorage.getItem('idExpediente');
            let idSesionxxx=localStorage.getItem('idSesion');

            this.capturaDocumentoService.postCompleteDocument(operation,forma,idExpedientexxx,idSesionxxx)
            .pipe(timeout(this.configService.config.private.frontServices.document.timeOut))
            .subscribe(() => {
              let response={
                status:200
              };
              anvProcesado = true;
              this.debugService.debug({
                console: true,
                alert: true,
                msjConsole: "INFO " + new Date() + " DOC_RESPONSE " + JSON.stringify(response),
                msjAlert: "DOC_RESPONSE " + response.status
              });
              if (response.status > 205) {
                this.modalService.sendModal({
                  open: true,
                  btnEnterado: true,
                  modalHeader: this.configService.config.public.modal_ErrorEnrolamiento_header,
                  modalBody: this.configService.config.public.modal_ErrorEnrolamiento_body,
                  txtEnterado: this.configService.config.public.modal_ErrorEnrolamiento_cierra,
                });
              }
              this.redirectService.sendStatus({ code: response.status, type: 'document' });
            }, response => {
              anvProcesado = true;
              response["status"]=500;
              const code = response.status;
              this.debugService.debug({
                console: true,
                alert: true,
                msjConsole: "INFO " + new Date() + " DOC:RESPONSE " + JSON.stringify(response),
                msjAlert: "DOC:RESPONSE " + code
              });
              if (code > 205) {
                this.modalService.sendModal({
                  open: true,
                  btnEnterado: true,
                  modalHeader: this.configService.config.public.modal_ErrorEnrolamiento_header,
                  modalBody: this.configService.config.public.modal_ErrorEnrolamiento_body,
                  txtEnterado: this.configService.config.public.modal_ErrorEnrolamiento_cierra,
                });
              }
              this.redirectService.sendStatus({ code: code, type: 'document' });
            });
          } else {
            this.debugService.debug({
              console: true,
              alert: true,
              msjConsole: "ERR " + new Date() + " csrf_undefined",
              msjAlert: "csrf_undefined"
            });
          }
        });
      }
    }
  }

  /**
   * Valida los eventos que proporciona la libreria  de veridas makeVDDocumentWidget
   * @param e tipo de vento enviado por libreria selfie
   */
  documentDetectionEvent(e) {
    if (e && e.type) {
      const operation = e.type;
      this.debugService.debug({
        console: true,
        alert: true,
        msjConsole: "INFO " + new Date() + " DOC_OPERACION_REALIZADA " + operation,
        msjAlert: "DOC_OPERACION_REALIZADA " + operation
      });
      this.configService.getFullscreen(document.documentElement, "landscape");
      switch (operation) {
        case 'VDDocument_obverseDetection':
          this.configService.getFullscreen(document.documentElement, "landscape");
          this.obverseDetection(e, operation);
          break;
        case 'VDDocument_reverseDetection':
          this.configService.getFullscreen(document.documentElement, "landscape");
          this.reverseDetection(e, operation);
          break;
        case 'VDDocument_mountFailure':
          this.configService.getFullscreen(document.documentElement, "unlock");
          this.router.navigate(['/bio/incompatible']);
          break;
        case 'VDDocument_cameraStarted':
          this.configService.getFullscreen(document.documentElement, "landscape");
          break;
        case 'VDDocument_cameraFailure':
          this.configService.getFullscreen(document.documentElement, "unlock");
          this.router.navigate(['/bio/permisos'], { skipLocationChange: true });
          break;
        case 'VDDocument_detectionTimeout':
          if (anvProcesado) {
            this.step.sendStep("timeoutReverso");
          }
          this.configService.getFullscreen(document.documentElement, "unlock");
          this.router.navigate(['/bio/timeOut'], { skipLocationChange: true });
          break;
      }
    }
  }
  /**
   * Metodo que envia subscripcion para abrir modal confirmacion
   */
  openModal() {
    this.modalService.sendModal({
      open: true,
      btnCancelaProceso: true,
      btnCierraModal: true,
      btnEnterado: false,
      modalHeader: this.configService.config.public.modal_CancelaEnrolamiento_header,
      modalBody: this.configService.config.public.modal_CancelaEnrolamiento_body,
      txtCancelaProceso: this.configService.config.public.modal_CancelaEnrolamiento_aceptar,
      txtCierraModal: this.configService.config.public.modal_CancelaEnrolamiento_cierra
    });
  }

  /**
   * Metodo que se suscribe al flujo de numero de intentos de captura documento, video o selfie
   */
  subscribeIntentos(): void {
    this.subscriptionIntentos$ = this.intentosService.getIntentos()
      .subscribe(intentos => {
        if (intentos && intentos.intentos === 0) {
          this.configService.getFullscreen(document.documentElement, "unlock");
          this.router.navigate(['/bio/intentos'], { skipLocationChange: true });
        }
      });
  }
  /**
   * Metodo para ejecutar unsubscribe del componente, y limpiar intervals
   */
  ngOnDestroy() {
    try {
      clearInterval(this.intervalCapture);
      clearInterval(this.intervalWASM);
      this.subscriptionIntentos$.unsubscribe();
      this.subscriptionCsrf$.unsubscribe();
      this.subscriptionDoc$.unsubscribe();
      this.subscriptionStep$.unsubscribe();
      this.subscriptionDato$.unsubscribe();

    } catch (err) { }
  }

}