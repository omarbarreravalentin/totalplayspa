import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { ModalService } from '../../../shared/services/modal.service';
import { ConfigService } from '../../../shared/services/config.service';
import { IntentosService } from '../../../shared/services/intentos.service';
import { RedirectService } from '../../../shared/services/redirect.service';
import { CsrfService } from '../../../shared/services/csrf.service';
import { Subscription } from 'rxjs';
import { CapturaFotoBioService } from '../../../shared/services/captura-foto.bio.service';
import { StepsService } from '../../../shared/services/steps.service';
import { timeout } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import { DebugService } from '../../../shared/services/debug.service';
import { CapturaDocumentoBioService } from '../../../shared/services/captura-documento.bio.service';
import { DatosService } from '../../../shared/services/datos.service';


/**
 * Constante para hacer refrencia a la libreria veridas makeVDSelfieWidget
 */
declare const makeVDSelfieWidget: any; // valores permitidos makeVDAliveWidget(), makeVDSelfieWidget()
/**
 * Constante para hacer refrencia a la libreria resumable
 */
declare const Resumable: any;
/**
 * Constante para conocer si la fotoselfie ya ha sido enviada
 */
let ciclof: boolean;
@Component({
  selector: 'scf-captura-foto',
  templateUrl: 'captura-foto.component.html',
  styleUrls: ['captura-foto.component.css']
})
/**
 * Clase para procesar la captura de la selfie bioonboarding, utiliza pluggin Resumable para dividir en partes la selfie
 */
export class CapturaFotoComponent implements OnInit, OnDestroy {
  /**
   * Variable para validar status del boton siguiente
   */
  public showButton: boolean;
  /**
   * Variable para  guardar mensajes de texto para modal
   */
  public fotoMsj: boolean;
  /**
   * Variable para concatener tipo de pluggin selfie
   */
  public sdk: string;
  /**
   * Variable para susbcribirse a servicio de numero de intentos al capturar selfie
   */
  public subscriptionIntentos$: Subscription;
  /**
   * Variable para almacenar intervalo de tiempo observable
   */
  public interval: any;
  /**
   * Variable para guardar intervalo de tiempo para la captura de la selfie
   */
  public intervalCapture: any;
  /**
   * Variable Subscription para guardar observable csrf
   */
  public subscriptionCsrf$: Subscription;
  /**
   * Variable Subscription para guardar observable csrf
   */
  public subscriptionSelfie$: Subscription;
  /**
   * Se inicializan los servicios que utilizara el componente, asi como asignacion de valores a avriables
   */
  constructor(
    private configService: ConfigService,
    private router: Router,
    private statusSpinner: SpinnerService,
    private intentosService: IntentosService,
    private modalService: ModalService,
    private redirectService: RedirectService,
    private capturaFotoService: CapturaFotoBioService,
    private csrfService: CsrfService,
    private deviceService: DeviceDetectorService,
    private debugService: DebugService,
    private capturaDocumentoService: CapturaDocumentoBioService,
    private datosService:DatosService,
    private step: StepsService) {
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });
    if (this.deviceService.isMobile()) {
      this.configService.getFullscreen(document.documentElement, "portrait");
    } else {
      this.configService.getFullscreen(document.documentElement, undefined);
    }
    this.statusSpinner.sendSpinner({ status: false });
    this.showButton = true;
    this.fotoMsj =true;
    ciclof = false;
  }
  /**
   * Metodo para inicializar llamada alibreria makeVDSelfieWidget, asi mismo genera todos los listener donde se escucharan
   * los estados de desarrolo de la captura del la selfie
   */
  ngOnInit() {
    this.subscribeIntentos();
    this.intervalCapture = setInterval(() => {
      const vdConfirmationx: any = document.getElementsByClassName('vd-button');
      if (vdConfirmationx && vdConfirmationx[0] && vdConfirmationx[0].innerHTML === this.configService.config.public.photo_repeatText) {
        const vdConfirmationxs = vdConfirmationx[0];
        vdConfirmationxs.onclick = () => {
          this.configService.getFullscreen(document.documentElement, "portrait");
          this.intentosService.sendIntentos(true);
        };
      }
    }, 500);
    this.sdk = 'VDSelfie'; // Valores permitidos VDSelfie, VDAlive
    const VDAlive = makeVDSelfieWidget(); // valores permitidos makeVDAliveWidget(), makeVDSelfieWidget()
    VDAlive({
      targetSelector: '#targetF',
      webrtcUnsupportedText: this.configService.config.public.photo_webrtcUnsupportedText,
      displayErrors: this.configService.config.public.photo_displayErrors,
      infoAlertShow: this.configService.config.public.photo_infoAlertShow,
      infoAlertPhotoSelfie: this.configService.config.public.photo_infoAlertPhotoSelfie,
      detectionTimeout: this.configService.config.public.photo_detectionTimeout,
      reviewImage: this.configService.config.public.photo_reviewImage,
      repeatText: this.configService.config.public.photo_repeatText,
      infoReviewImageText: this.configService.config.public.photo_infoReviewImageText,
      continueText: this.configService.config.public.photo_continueText,
      sdkBackgroundColorInactive: this.configService.config.public.photo_sdkBackgroundColorInactive,
      borderColorCenteringAidInactive: this.configService.config.public.photo_borderColorCenteringAidInactive,
      borderColorCenteringAidDefault: this.configService.config.public.photo_borderColorCenteringAidDefault,
      borderColorCenteringAidDetecting: this.configService.config.public.photo_borderColorCenteringAidDetecting,
      borderColorCenteringAidDetectingSuccess: this.configService.config.public.photo_borderColorCenteringAidDetectingSuccess,
      outerGlowCenteringAidDefault: this.configService.config.public.photo_outerGlowCenteringAidDefault,
      outerGlowCenteringAidDetecting: this.configService.config.public.photo_outerGlowCenteringAidDetecting,
      confirmationDialogBackgroundColor: this.configService.config.public.photo_confirmationDialogBackgroundColor,
      confirmationDialogTextColor: this.configService.config.public.photo_confirmationDialogTextColor,
      confirmationDialogButtonTextColor: this.configService.config.public.photo_confirmationDialogButtonTextColor,
      confirmationDialogLinkTextColor: this.configService.config.public.photo_confirmationDialogLinkTextColor,
      confirmationDialogButtonBackgroundColor: this.configService.config.public.photo_confirmationDialogButtonBackgroundColor,
      setLandscapeDeviceMessage: this.configService.config.public.photo_setLandscapeDeviceMessage,
      setPortraitDeviceMessage: this.configService.config.public.photo_setPortraitDeviceMessage,
      smileRequestSmile: this.configService.config.public.photo_smileRequestSmile,
      smileRequestSerious: this.configService.config.public.photo_smileRequestSerious,
      fitYourFace: this.configService.config.public.photo_fitYourFace,
      confirmationColorTick: this.configService.config.public.document_confirmationColorTick
    });
    const detectionEvents = [
      this.sdk + '_mounted',
      this.sdk + '_unmounted',
      this.sdk + '_mountFailure',
      this.sdk + '_cameraStarted',
      this.sdk + '_cameraFailure',
      this.sdk + '_faceDetection',
      this.sdk + '_detectionTimeout'
    ];

    detectionEvents.forEach((event) => {
      addEventListener(event, this.fotoDetectionEvent.bind(this), true);
    });
  }
  /**
   * Valida los eventos que proporciona la libreria  de veridas makeVDSelfieWidget
   * @param e tipo de vento enviado por libreria selfie
   */
  fotoDetectionEvent(e) {
    if (e && e.type) {
      const operation = e.type;
      this.debugService.debug({
        console: true,
        alert: true,
        msjConsole: "INFO " + new Date() + " SELFIE_OPERACION_REALIZADA " + operation,
        msjAlert: "SELFIE_OPERACION_REALIZADA " + operation
      });
      switch (operation) {
        case this.sdk + '_faceDetection':
          if (!ciclof) {
            ciclof = true;
            this.subscriptionCsrf$ = this.csrfService.getStatus().subscribe(datacsrf => {
              if (datacsrf && datacsrf.token) {
                const codecsrf = datacsrf.token;
                const dataurl = e.detail;
                const arr = dataurl.split(',');
                const mime = arr[0].match(/:(.*?);/)[1];
                const bstr = atob(arr[1]);
                let n = bstr.length;
                const u8arr = new Uint8Array(n);
                while (n--) {
                  u8arr[n] = bstr.charCodeAt(n);
                }
                const fileToUpload = new File([u8arr], 'fileUpload.png', { type: mime });
                
                let forma = new FormData();
                forma.append("file", fileToUpload, fileToUpload.name);
                forma.append('type', operation);

                this.statusSpinner.sendSpinner({ status: true, txtSpinner: 'Estamos trabajando...',subtxtSpinner:'Espera un poco.'});
                
                let idExpedientexxx=localStorage.getItem('idExpediente');
                let idSesionxxx=localStorage.getItem('idSesion');
    
                this.capturaDocumentoService.postCompleteDocument(operation,forma,idExpedientexxx,idSesionxxx)
                  .pipe(timeout(this.configService.config.private.frontServices.document.timeOut))
                  .subscribe(() => {
                    this.intentosService.sendIntentos(false);
                    this.router.navigate(['/bio/infoVideo']);
                  }, response => {
                    this.modalService.sendModal({
                      open: true,
                      btnEnterado: true,
                      modalHeader: this.configService.config.public.modal_ErrorEnrolamiento_header,
                      modalBody: this.configService.config.public.modal_ErrorEnrolamiento_body,
                      txtEnterado: this.configService.config.public.modal_ErrorEnrolamiento_cierra,
                    });
                  });
              }
            });
          }
          break;

        case this.sdk + '_mounted':
        case this.sdk + '_cameraStarted':
          this.interval = setInterval(time => {
            const detection: any = document.querySelector('.targetf .vd-detection-modal-content.vd-confirmation .vd-image-frame img');
            if (detection && detection.src.length > 0) {
              this.showButton = false;
              this.fotoMsj = false;
            } else {
              this.showButton = true;
              this.fotoMsj = true;
            }
          }, 500);
          break;

        case this.sdk + '_mountFailure':
          this.router.navigate(['/bio/incompatible'], { skipLocationChange: true });
          break;

        case this.sdk + '_cameraFailure':
          this.router.navigate(['/bio/permisos'], { skipLocationChange: true });
          break;

        case this.sdk + '_detectionTimeout':
          this.configService.getFullscreen(document.documentElement, "unlock");
          this.router.navigate(['/bio/timeOut'], { skipLocationChange: true });
          break;

        default:
          break;
      }
    }
  }
  /**
   * Metodo que envia subscripcion para abrir modal confirmacion
   */
  openModal() {
    this.modalService.sendModal({
      open: true,
      btnCancelaProceso: true,
      btnCierraModal: true,
      btnEnterado: false,
      modalHeader: this.configService.config.public.modal_CancelaEnrolamiento_header,
      modalBody: this.configService.config.public.modal_CancelaEnrolamiento_body,
      txtCancelaProceso: this.configService.config.public.modal_CancelaEnrolamiento_aceptar,
      txtCierraModal: this.configService.config.public.modal_CancelaEnrolamiento_cierra
    });
  }
  /**
   * Metodo que se suscribe al flujo de numero de intentos de captura documento, video o selfie
   */
  subscribeIntentos(): void {
    this.subscriptionIntentos$ = this.intentosService.getIntentos()
      .subscribe(intentos => {
        if (intentos && intentos.intentos === 0) {
          this.configService.getFullscreen(document.documentElement, "portrait");
          this.router.navigate(['/bio/intentos'], { skipLocationChange: true });
        }
      });
  }
  /**
   * Metodo para ejecutar unsubscribe del componente y limpiar intervals
   */
  ngOnDestroy(): void {
    try {
      clearInterval(this.interval);
      clearInterval(this.intervalCapture);
      this.subscriptionIntentos$.unsubscribe();
      this.subscriptionCsrf$.unsubscribe();
      this.subscriptionSelfie$.unsubscribe();
    } catch (err) { }
  }
}
