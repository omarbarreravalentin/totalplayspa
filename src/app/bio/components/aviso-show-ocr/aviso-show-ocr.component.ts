import { Component } from '@angular/core';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { Router } from '@angular/router';
import { StepsService } from '../../../shared/services/steps.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ConfigService } from '../../../shared/services/config.service';
import { BioService } from '../../../shared/services/bio.service';
import { timeout } from 'rxjs/operators';
import { RedirectService } from '../../../shared/services/redirect.service';
import * as _ from 'underscore';

@Component({
  selector: 'obv-aviso-show-ocr',
  templateUrl: 'aviso-show-ocr.component.html'
})
/**
 * Clase para mostrar aviso de captura del video veridas
 */
export class AvisoShowOCR {
  public show_all_data = this.configService.config.public.show_all_data;
  public show_error=false;

  constructor(private spinnerService: SpinnerService,
    private route: Router,
    private statusSpinner: SpinnerService,
    private deviceService: DeviceDetectorService,
    private configService: ConfigService,
    private redirectService: RedirectService,
    private bio: BioService,
    private step: StepsService) {
    this.statusSpinner.sendSpinner({ status: true, txtSpinner: 'Espera un poco', subtxtSpinner: 'Leyendo tus datos' });
    setTimeout(() => {
      let idExpedientexxx = localStorage.getItem('idExpediente');
      let idSesionxxx = localStorage.getItem('idSesion');
      this.bio.getIdValidas(idExpedientexxx, idSesionxxx)
        .pipe(timeout(this.configService.config.private.frontServices.document.timeOut))
        .subscribe((response) => {
          if (response.body.idValidas) {
            let idValidas = response.body.idValidas;
            localStorage.setItem('idValidas', idValidas);
            //obtener OCR
            this.bio.getOCR(idValidas, {})
              .pipe(timeout(this.configService.config.private.frontServices.document.timeOut))
              .subscribe((response) => {
                if (response && response["data"] && response["data"]["nodes"]) {
                  let resultado = response["data"]["nodes"];
                  if (this.show_all_data) {
                    this.addElementToList("ListSalidaOCR", resultado, "auto");
                  } else {
                    document.getElementById('nombre').innerHTML = resultado[0].text;
                    document.getElementById('apellidos').innerHTML = resultado[1].text;
                  }

                  //obtener SCORES
                  this.bio.getSCORES(idValidas, {})
                    .pipe(timeout(this.configService.config.private.frontServices.document.timeOut))
                    .subscribe((response) => {
                      if (response && response["data"] && response["data"]["biometryScores"]) {
                        this.statusSpinner.sendSpinner({ status: false });
                        let resultado = response["data"]["biometryScores"];
                        let documentScores = response["data"]["documentScores"];
                        let documentGlobal=documentScores[144].value;
                        let scoreSelfie = resultado[0].value;
                        let scoreDuplicateAttack = resultado[2].value;
                        let scoreSelfieAuth = resultado[3].value;
                        let ValidasScoreLifeProof=resultado[4].value;
                        let ValidasScoreVideo=resultado[5].value;

                        if (this.show_all_data) {
                          this.addElementToList("ListSalidaSCORES", resultado, "auto");
                        } else {  
                          let order = [
                            {name: 'Calificación global del documento:', score: documentGlobal.toFixed(6)},
                            {name: 'Probabilidad de que la selfie sea auténtica y no falsificada:', score: scoreSelfieAuth.toFixed(6)},
                            {name: 'Score principal con el análisis de similaridad entre la selfie y la foto del documento (INE):', score: scoreSelfie.toFixed(6)},
                            {name: 'Probabilidad de que la foto de la INE sea diferente a la foto de la selfie:', score: scoreDuplicateAttack.toFixed(6)},
                            {name: 'Probabilidad de que la persona que sale en el video este viva:', score: ValidasScoreLifeProof.toFixed(6)},
                            {name: 'Probabilidad de que la foto de la selfie y la persona del video, corresponda a la misma persona:', score: ValidasScoreVideo.toFixed(6)}
                          ];
                          //let order=_.sortBy(stooges, 'score');
                          for (let index = 0; index < order.length; index++) {
                            document.getElementById('title'+index).innerHTML = order[index].name;
                            document.getElementById('data'+index).innerHTML = order[index].score;
                          }
                        }
                      } else {
                        this.show_all_data=true;
                        this.statusSpinner.sendSpinner({ status: false });
                        this.show_error=true;
                        //this.addElementToList("ListSalidaSCORES", { error: "Error al obtener datos" }, "auto");
                      }
                    }, response => {
                      this.show_all_data=true;
                      this.statusSpinner.sendSpinner({ status: false });
                      this.show_error=true;
                      //this.addElementToList("ListSalidaSCORES", { error: "UpsError al obtener datos" }, "auto");
                    });
                } else {
                  this.show_all_data=true;
                  this.statusSpinner.sendSpinner({ status: false });
                  this.show_error=true;
                  //this.addElementToList("ListSalidaOCR", { error: "Error al obtener datos" }, "auto");
                }
              }, response => {
                this.show_all_data=true;
                this.statusSpinner.sendSpinner({ status: false });
                this.show_error=true;
                //this.addElementToList("ListSalidaOCR", { error: "UpsError al obtener datos" }, "auto");
              });
          } else {
            this.show_all_data=true;
            this.statusSpinner.sendSpinner({ status: false });
            this.show_error=true;
            //this.addElementToList("ListSalidaOCR", { error: "Hubo un error al procesar su solicitud" }, "auto");
          }
        }, response => {
          this.statusSpinner.sendSpinner({ status: false });
          this.step.sendStep("final");
          window.location.href = 'https://www.minsait.com/es/?statusCode=-1';
        });
    }, 30000);
  }

  addElementToList(lista, msj, altura) {
    var ul = document.getElementById(lista);
    var li = document.createElement("li");
    var back = ["red", "yellow", "yel", "grey", "green"];
    var rand = back[Math.floor(Math.random() * back.length)];
    var children = ul.children.length + 1
    li.setAttribute("id", "element" + children)
    li.setAttribute("class", "list-group-item licompleto " + rand)
    if (altura != undefined)
      li.setAttribute("style", "height:" + altura)
    if (lista == "ListSalidaConexion") {
      li.appendChild(document.createTextNode("" + msj));
    } else {
      li.innerHTML = this.syntaxHighlight(msj);
    }
    ul.appendChild(li)
  }

  syntaxHighlight(json) {
    if (typeof json != 'string') {
      json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
      var cls = 'number';
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          cls = 'key';
        } else {
          cls = 'string';
        }
      } else if (/true|false/.test(match)) {
        cls = 'boolean';
      } else if (/null/.test(match)) {
        cls = 'null';
      }
      if (cls == "string") {
        return '<span class="' + cls + '">' + match + '</span>';
      } else if (cls == "boolean") {
        return '<span style="color:blue">' + match + '</span>';
      }
      else if (cls == "number") {
        return '<span style="color:green">' + match + '</span>';
      }
      else if (match == "fieldName" || cls == "fieldName") {
        return '<br><span style="color:blue">' + match + '</span>';
      }
      else {
        return '<br><span style="color:red">' + match + '</span>';
      }
    });
  }


  terminar(){
    this.redirectService.sendStatus({ code: 500, type: 'document' });
  }


}
