import { Component, OnInit, OnDestroy } from '@angular/core';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ConfigService } from '../../../shared/services/config.service';
import { StepsService } from '../../../shared/services/steps.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'sacf-aviso-captura-foto',
  templateUrl: 'aviso-captura-foto.component.html'
})
/**
 * Clase para mostrar aviso de captura selfie veridas
 */
export class AvisoCapturaFotoComponent implements OnInit, OnDestroy {

  /**
   * Variable para susbcribirse a servicio del paso actual
   */
  public subscriptionStep$: Subscription;

  /**
   * Se inicializan los servicios que utilizara el componente, asi como asignacion de valores a avriables
   */
  constructor(
    private configService: ConfigService,
    private deviceService: DeviceDetectorService,
    private spinnerService: SpinnerService,
    private router: Router,
    private step: StepsService) {
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });
    this.spinnerService.sendSpinner({ status: false });
    if (this.deviceService.isMobile()) {
      this.configService.getFullscreen(document.documentElement, "portrait");
    } else {
      this.configService.getFullscreen(document.documentElement, undefined);
    }
    this.subscriptionStep$ = this.step.getStep().subscribe(paso => {
      if (paso) {
        switch (paso) {
          case "captureFoto":
            this.router.navigate(['/bio/captureFoto'], { skipLocationChange: true });
            break;
          case "captureVideo":
            this.router.navigate(['/bio/captureVideo'], { skipLocationChange: true });
            break;
        }
      }
    });
  }

  /**
  * Metodo para redirigir a capturaSelfie
  */
  captureSelfie() {
    this.configService.getFullscreen(document.documentElement, "portrait");
    this.step.sendStep("captureFoto");
    this.router.navigate(['/bio/captureFoto'], { queryParams: { date: new Date().getTime() } });
  }

  /**
 * Metodo para ejecutar unsubscribe del componente, y limpiar intervals
 */
  ngOnDestroy() {
    try {
      this.subscriptionStep$.unsubscribe();
    } catch (error) { }
  }

  /**
   * Metodo para inicializar llamada alibreria makeVDSelfieWidget, asi mismo genera todos los listener donde se escucharan
   * los estados de desarrolo de la captura del la selfie
   */
  ngOnInit() {
    if (this.deviceService.isMobile()) {
      this.configService.getFullscreen(document.documentElement, "portrait");
    } else {
      this.configService.getFullscreen(document.documentElement, undefined);
    }
  }
}
