import { Component } from '@angular/core';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { Router } from '@angular/router';
import { StepsService } from '../../../shared/services/steps.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ConfigService } from '../../../shared/services/config.service';

@Component({
  selector: 'sacv-aviso-captura-video',
  templateUrl: 'aviso-captura-video.component.html'
})
/**
 * Clase para mostrar aviso de captura del video veridas
 */
export class AvisoCapturaVideoComponent {
  constructor(private spinnerService: SpinnerService,
    private route: Router,
    private deviceService: DeviceDetectorService,
    private configService: ConfigService,
    private step: StepsService) {
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });
    if (this.deviceService.isMobile()) {
      this.configService.getFullscreen(document.documentElement, "portrait");
    } else {
      this.configService.getFullscreen(document.documentElement, undefined);
    }
    this.spinnerService.sendSpinner({ status: false });
  }

  /**
   * Metodo para redirigir a capturaVideo
   */
  captureVideo() {
    this.configService.getFullscreen(document.documentElement, "portrait");
    this.step.sendStep("captureVideo");
    this.route.navigate(['/bio/captureVideo'], { queryParams: { date: new Date().getTime() } });
  }
}
