import { Component } from '@angular/core';
import { RedirectService } from '../../../shared/services/redirect.service';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { StepsService } from '../../../shared/services/steps.service';

@Component({
  selector: 'saim-aviso-intentos-maximo',
  templateUrl: 'aviso-intentos-maximo.component.html'
})
/**
 * Clase para mostrar el aviso de numero maximo de intentos
 */
export class AvisoIntentosMaximoComponent {
  /**
   * Inicializa servicio  RedirectService
   */
  constructor(private redirectService: RedirectService,
    private spinnerService: SpinnerService,
    private step: StepsService) {
    // DESACTIVA EL BTN RETROCEDER
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });

    this.step.sendStep("limitAttemps");
    this.spinnerService.sendSpinner({ status: false });
  }
  /**
   * Envio a servicio peticion http 505 para regresra al comienzo
   */
  terminar() {
    this.redirectService.sendStatus({ code: 505 });
  }
}
