import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { IntentosService } from '../../../shared/services/intentos.service';
import { RedirectService } from '../../../shared/services/redirect.service';
import { Subscription } from 'rxjs';
import { StepsService } from '../../../shared/services/steps.service';

@Component({
  selector: 'sato-aviso-time-out.component',
  templateUrl: 'aviso-time-out.component.html'
})
/**
 * Clase para mostrar el aviso de tiempo expiracion para el aplicativo
 */
export class AvisoTimeOutComponent implements OnInit, OnDestroy {
  /**
   * Variable para almacenar numero de intentos
   */
  intentos: number;
  /**
   * Variable para susbcribirse a servicio de numero de intentos al capturar selfie
   */
  public subscriptionIntentos$: Subscription;
  /**
   * Variable para susbcribirse a servicio del paso actual
   */
  public subscriptionStep$: Subscription;
  /**
   * Inicializa servicios para validar numero de intentos
   */
  constructor(private router: Router,
    private intentosService: IntentosService,
    private redirectService: RedirectService,
    private step: StepsService) {
    // DESACTIVA EL BTN RETROCEDER
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });
    
    this.intentosService.sendIntentos(true);
  }
  /**
   * Inicializa la subcripcion obserbavle numero de intentos
   */
  ngOnInit() {
    this.subscribeIntentos();
  }
  /**
   * Metodo para regresar boton atras navegador
   */
  timeOut(): void {
    this.subscriptionIntentos$ = this.intentosService.getIntentos()
      .subscribe(intentos => {
        if (intentos && intentos.intentos === 0) {
          this.redirectService.sendStatus({ code: 505 });
        } else {
          this.subscriptionStep$ = this.step.getStep().subscribe(paso => {
            if (paso) {
              switch (paso) {
                case "captureFoto":
                  this.router.navigate(['/bio/captureFoto'], { skipLocationChange: true });
                  break;
                case "captureDocs":
                case "timeoutReverso":
                  this.router.navigate(['/bio/captureDocuments'], { skipLocationChange: true });
                  break;
                case "captureVideo":
                  this.router.navigate(['/bio/captureVideo'], { skipLocationChange: true });
                  break;
                case "infoFoto":
                  this.router.navigate(['/bio/infoFoto'], { skipLocationChange: true });
                  break;
              }
            }
          });
        }
      });
  }
  /**
   * Metodo que se suscribe al flujo de numero de intentos de captura documento, video o selfie
   */
  subscribeIntentos(): void {
    this.subscriptionIntentos$ = this.intentosService.getIntentos()
      .subscribe(intentos => {
        if (intentos && intentos.intentos === 0) {
          this.router.navigate(['/bio/intentos'], { skipLocationChange: true });
        }
      });
  }

  /**
  * Metodo para ejecutar unsubscribe del componente, y limpiar intervals
  */
  ngOnDestroy() {
    try {
      this.subscriptionStep$.unsubscribe();
      this.subscriptionIntentos$.unsubscribe();
    } catch (error) { }
  }
}
