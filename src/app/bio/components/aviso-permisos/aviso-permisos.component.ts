import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ModalService } from '../../../shared/services/modal.service';
import { SpinnerService } from '../../../shared/services/spinner.service';
import { StepsService } from '../../../shared/services/steps.service';
import { ConfigService } from '../../../shared/services/config.service';
import { DebugService } from '../../../shared/services/debug.service';

/*Constante de tipo window.navigator*/
declare const navigator: any;

@Component({
  selector: 'sap-aviso-permisos',
  templateUrl: 'aviso-permisos.component.html'
})
/**
 * Clase para mostrar el componente aviso de permisos para la camara
 */
export class AvisoPermisosComponent implements OnDestroy {
  /**
   * Variable para validar si es un navegador compatible
   */
  deviceInfo = null;
  /**
 * Variable para validar los permisos usando un intervalo de tiempo
 */
  public intervalPerm: any;
  /**
   * Variable para confirmar los permisos
   */
  confirmPermissions = false;
  /**
  * Variable para susbcribirse a servicio del paso actual
  */
  public subscriptionStep$: Subscription;
  /**
   * Inicializa servicios para validar numero de intentos
   */
  constructor(private modalService: ModalService,
    private spinnerService: SpinnerService,
    private step: StepsService,
    private configService: ConfigService,
    private deviceService: DeviceDetectorService,
    private debugService: DebugService,
    private router: Router) {

    // DESACTIVA EL BTN RETROCEDER
    history.pushState(null, null, window.location.href);
    window.addEventListener('popstate', function (event) {
      history.pushState(null, document.title, window.location.href);
    });

    // DESACTIVA EL SPINNER
    this.spinnerService.sendSpinner({ status: false });

    // REVISA CADA X SEGUNDOS LOS PERMISOS, YA QUE EL USUARIO DIO CLICK EN EL ENLACE
    this.intervalPerm = setInterval(() => {
      // if (this.confirmPermissions == true) {

    const constraints = {
        video: {
          facingMode: (false ? "user" : "environment"),
          width: {
            min: 1024,
            ideal: 1920,
            max: 1920
          },
          height: {
            min: 720,
            ideal: 1080,
            max: 1080
          }
        },
        audio: true
      };

      if (navigator.mediaDevices === undefined) {
        navigator.mediaDevices = {};
      }

      if (navigator.mediaDevices.getUserMedia === undefined) {
        navigator.mediaDevices.getUserMedia = this.promisifiedOldGUM;
      }

      navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
        clearInterval(this.intervalPerm);
        this.subscriptionStep$ = this.step.getStep().subscribe(paso => {
          if (paso) {
            switch (paso) {
              case "captureFoto":
                this.router.navigate(['/bio/captureFoto'], { skipLocationChange: true });
                break;
              case "captureDocs":
                this.configService.getFullscreen(document.documentElement, "landscape");
                this.router.navigate(['/bio/captureDocuments'], { skipLocationChange: true });
                break;
              case "captureVideo":
                this.router.navigate(['/bio/captureVideo'], { skipLocationChange: true });
                break;
              case "infoFoto":
                this.router.navigate(['/bio/infoFoto'], { skipLocationChange: true });
                break;
              default:
                window.history.back();
                break;
            }
          }
        });
      })
        .catch((err) => {
          this.debugService.debug({
            console: true,
            alert: true,
            msjConsole: "ERR " + new Date() + " ERR_PERMISIONS :" + err.name + " - " + err.message,
            msjAlert: "ERR_PERMISIONS :" + err.name
          });
          switch (err.name) {
            case "NotAllowedError":
            case "PermissionDeniedError":
              break;
            case "OverconstrainedError":
            case "NotReadableError":
              clearInterval(this.intervalPerm);
              this.modalService.sendModal({
                open: true,
                modalHeader: this.configService.config.public.modal_NotReadableError_header,
                modalBody: this.configService.config.public.modal_NotReadableError_body,
                btnCierraModal: true,
                txtCierraModal: this.configService.config.public.modal_NotReadableError_btn,
              });
              break;
            default:
              clearInterval(this.intervalPerm);
              this.modalService.sendModal({
                open: true,
                modalHeader: 'Error',
                modalBody: err.message,
                btnCierraModal: true,
                txtCierraModal: this.configService.config.public.modal_NotAllowedError_btn
              });
              break;
          }
        });
      // }
    }, 1000);
  }

  /**
   * Metodo para detectar si el dispositivo tiene permisos al audio y la camara
   */
  trypermisos(): void {
    const constraints = {
      video: {
        facingMode: (false ? "user" : "environment"),
        width: {
          min: 1024,
          ideal: 1920,
          max: 1920
        },
        height: {
          min: 720,
          ideal: 1080,
          max: 1080
        }
      },
      audio: true
    };

    if (navigator.mediaDevices === undefined) {
      navigator.mediaDevices = {};
    }

    if (navigator.mediaDevices.getUserMedia === undefined) {
      navigator.mediaDevices.getUserMedia = this.promisifiedOldGUM;
    }

    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
      clearInterval(this.intervalPerm);
      this.subscriptionStep$ = this.step.getStep().subscribe(paso => {
        if (paso) {
          switch (paso) {
            case "captureFoto":
              this.router.navigate(['/bio/captureFoto'], { skipLocationChange: true });
              break;
            case "captureDocs":
              this.configService.getFullscreen(document.documentElement, "landscape");
              this.router.navigate(['/bio/captureDocuments'], { skipLocationChange: true });
              break;
            case "captureVideo":
              this.router.navigate(['/bio/captureVideo'], { skipLocationChange: true });
              break;
            case "infoFoto":
              this.router.navigate(['/bio/infoFoto'], { skipLocationChange: true });
              break;
            default:
              window.history.back();
              break;
          }
        }
      });
    })
      .catch((err) => {
        this.deviceInfo = this.deviceService.getDeviceInfo();
        const os = this.deviceInfo.os;
        if (os == "Android") {
          window.open(this.configService.config.public.urlAndroidPermissions);
        } else if (os == "Windows") {
          window.open(this.configService.config.public.urlWindowsPermissions);
        } else {
          window.open(this.configService.config.public.urlIosPermissions);
        }
        this.debugService.debug({
          console: true,
          alert: true,
          msjConsole: "ERR " + new Date() + " ERR_PERMISIONS :" + err.name + " - " + err.message,
          msjAlert: "ERR_PERMISIONS :" + err.name
        });
        this.confirmPermissions = true;
      });
  }

  /**
   * Metodo para enviar parametros al modal y abrirlo para mandar mensaje personalizado
   */
  openModal() {
    this.modalService.sendModal({
      open: true,
      modalHeader: this.configService.config.public.modal_CancelaEnrolamiento_header,
      modalBody: this.configService.config.public.modal_CancelaEnrolamiento_body,
      btnCancelaProceso: true,
      btnCierraModal: true,
      btnEnterado: false,
      txtCancelaProceso: this.configService.config.public.modal_CancelaEnrolamiento_aceptar,
      txtCierraModal: this.configService.config.public.modal_CancelaEnrolamiento_cierra
    });
  }

  /**
  * Metodo para ejecutar unsubscribe del componente, y limpiar intervals
  */
  ngOnDestroy() {
    try {
      clearInterval(this.intervalPerm);
      this.subscriptionStep$.unsubscribe();
    } catch (error) { }
  }

  /**
   * Método que valida UserMedia para antiguos browser
   * @param constraints parametros de permisos camara-microfono
   * @param successCallback funcion en caso de ser exitosa la asignacion
   * @param errorCallback error en caso de no poder asignar los permisos
   * @returns  promise de los permisos
   */
  promisifiedOldGUM(constraints, successCallback, errorCallback) {

    // First get ahold of getUserMedia, if present
    let getUserMedia = (navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia);
    // Some browsers just don't implement it - return a rejected promise with an error
    // to keep a consistent interface
    if (!getUserMedia) {
      return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
    }
    // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
    return new Promise(function (successCallbackx, errorCallbackx) {
      getUserMedia.call(navigator, constraints, successCallback, errorCallback);
    });
  }
}
