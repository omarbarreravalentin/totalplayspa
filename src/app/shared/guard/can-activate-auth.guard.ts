import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, RouterStateSnapshot, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { StatusService } from '../services/status.service';
import { DebugService } from '../services/debug.service';

import { Subscription } from 'rxjs';
@Injectable()
/**
 * Clase que provee las validaciones para acceder a las rutas del front
 */
export class CanActivateAuthGuard implements CanActivate {
    /**
     * variable para subcribirse a status del router del aplicativo
     */
    subscriptionStatus$: Subscription;
    /**
     * Variable para almacenar arreglos de rutas a validar
     */
    statusUser = [
        '/bio',
        '/bio/captureDocuments',
        '/bio/infoFoto',
        '/bio/captureFoto',
        '/bio/infoVideo',
        '/bio/captureVideo',
        '/bio/final'
    ];
    /**
     * Variable boolean para validar si es correcto ingresar a una ruta del aplicativo
     */
    activate = false;
    /**
     * constructor que inicializa servicio router, route y statusService
     */
    constructor(private router: Router, private route: ActivatedRoute, private statusService: StatusService,    private debugService: DebugService) { }
    /**
     * Metodo para validar rutas del aplicativo, si proveniene del paso anterior los deja pasar
     */
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
        this.subscriptionStatus$ = this.statusService.getStatus()
            .subscribe(status => {
                if (status) {
                    this.statusUser.forEach((val, index) => {
                        if (status === val && state.url === this.statusUser[index + 1]) {
                            this.activate = true;
                        }
                    });
                }else{
                    this.debugService.debug({
                        console: true,
                        alert: true,
                        msjConsole: "INFO " + new Date() + " AUTH_GUARD_UNDEFINED",
                        msjAlert: "AUTH_GUARD_UNDEFINED"
                    });
                    // this.activate = true;
                }
            });
        if (this.activate) {
            return true;
        } else {
            this.debugService.debug({
                console: true,
                alert: true,
                msjConsole: "INFO " + new Date() + " REDIRECCIONANDO_AUTH_GUARD",
                msjAlert: "REDIRECCIONANDO_AUTH_GUARD"
            });
            this.router.navigate(['/']);
            return false;
        }
    }
}
