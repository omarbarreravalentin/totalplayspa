import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ModalService } from '../services/modal.service';
import { RedirectService } from '../services/redirect.service';
import { DebugService } from '../services/debug.service';
import { Router } from '@angular/router';
@Component({
  selector: 'sm-modal',
  templateUrl: 'modal.html'
})
/**
 * Clase para mostrar los diferentes mensajes del aplicativo
 */
export class ModalComponent implements OnInit {
  /**
   * Constante guradar texto confirmacion
   */
  modalHeader = 'Confirmación';
  /**
   * Constante pregunta de confirmacion
   */
  modalBody = '¿Estas seguro que deseas salir de la pagina?';
  /**
   * Bandera para mostrar/ocultar boton cancelar
   */
  btnCancelaProceso = false;
  /**
   * Bandera para mostrar/ocultar boton cerra modal
   */
  btnCierraModal = false;
  /**
   * Bandera para mostrar/ocultar boton confirmacion
   */
  btnEnterado = false;
  /**
   * constante para almacenar texto cancelar
   */
  txtCancelaProceso = 'Cancelar';
  /**
   * constante para almacenar texto cancelar
   */
  txtCierraModal = 'Cancelar';
  /**
   * constante para almacenar texto ok
   */
  txtEnterado = 'OK';
  /**
   * Variable para guardar subscripcion al modal
   */
  public subscriptionModal$: Subscription;
  /**
   * Instancia servicios a modal, y redirect
   */
  constructor(
    public activeModal: NgbActiveModal,
    private modalService: ModalService,
    private router: Router,
    private debugService: DebugService,
    private redirectService: RedirectService) {
  }
  /**
   * Inicializa subcripcion a obtencion de datos modal
   */
  ngOnInit() {
    this.subscribeModal();
  }
  /**
   * Metodo para validar destino del los componentes del modal
   * @param origen  - header body o boton del modal
   * @param destino - variable a pasar el valor
   */
  assignifContent(origen: any, destino: any) {
    if (origen) {
      destino = origen;
    }
    return destino;
  }
  /**
   * Metodo cerrar el modal y limpiar localstorage
   */
  cancelProcess(): void {
    this.activeModal.close();
    localStorage.clear();
    this.debugService.debug({
      console: true,
      msjConsole: "ERR " + new Date() + " PROCESO_CANCELADO_USUARIO"
    });
    this.redirectService.sendStatus({ code: 505 });
  }
  /**
   * Metodo para subcribirse al observable del modal para recibir los parametros que se
   * e informacion que se mostrara.
   */
  subscribeModal(): void {
    this.subscriptionModal$ = this.modalService.getModal()
      .subscribe(modal => {
        if (modal && modal.open === true) {
          this.modalHeader = this.assignifContent(modal.modalHeader, this.modalHeader);
          this.modalBody = this.assignifContent(modal.modalBody, this.modalBody);
          this.btnCancelaProceso = this.assignifContent(modal.btnCancelaProceso, this.btnCancelaProceso);
          this.btnCierraModal = this.assignifContent(modal.btnCierraModal, this.btnCierraModal);
          this.btnEnterado = this.assignifContent(modal.btnEnterado, this.btnEnterado);
          this.txtCancelaProceso = this.assignifContent(modal.txtCancelaProceso, this.txtCancelaProceso);
          this.txtCierraModal = this.assignifContent(modal.txtCierraModal, this.txtCierraModal);
          this.txtEnterado = this.assignifContent(modal.txtEnterado, this.txtEnterado);
        }
      });
  }
}
