import { animate, group, query, style, transition, trigger } from '@angular/animations';
/** 
 * constante slideAnimation principal para generar animacion
 */
export const SlideAnimation = trigger('SlideAnimation', [
  transition(':increment', group([
    query(':enter', [
      style({
        transform: 'translateX(100%)'
      }),
      animate('0.5s ease-out', style('*'))
    ]),
    query(':leave', [
      animate('0.5s ease-out', style({
        transform: 'translateX(-100%)'
      }))
    ])
  ])),
  transition(':decrement', group([
    query(':enter', [
      style({
        transform: 'translateX(-100%)'
      }),
      animate('0.5s ease-out', style('*'))
    ]),
    query(':leave', [
      animate('0.5s ease-out', style({
        transform: 'translateX(100%)'
      }))
    ])
  ]))
]);
