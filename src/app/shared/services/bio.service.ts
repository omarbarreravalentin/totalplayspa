import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { map } from 'rxjs/operators';
import { ConfigService } from './config.service';
@Injectable()
/**
 * Clase que provee los servicios http
 */
export class BioService {
  /**
   * Almacena la url base proveniente del Yamel para consumir los servicios get expediente
   */
  private urlGetExpediente: string;
  private urlGetIdValidas:string;
  /**
   * Almacena la url base proveniente del Yamel para consumir los servicios check token
   */
  private urlCheckToken: string;

  /**
   * Almacena la url base proveniente del Yamel para consumir los servicios get token
   */
  private urlGetToken: string;
  private urlGetSCORES:string;
  private urlGetOCR: string;

  /**
   * Inicialiazion del servicio http asi como la obtencion de las url base
   */
  constructor(
    private httpClient: HttpClient, private configService: ConfigService
  ) {
    this.urlGetExpediente = this.configService.config.private.frontServices.server + this.configService.config.private.frontServices.expediente.get;
    this.urlGetIdValidas = this.configService.config.private.frontServices.server + this.configService.config.private.frontServices.expediente.getIdValidas;
    this.urlCheckToken = this.configService.config.private.frontServices.server + this.configService.config.private.frontServices.token.check;
    this.urlGetToken = this.configService.config.private.frontServices.server + this.configService.config.private.frontServices.token.get;
    this.urlGetOCR=this.configService.config.private.frontServices.server + this.configService.config.private.frontServices.document.getOCR;
    this.urlGetSCORES=this.configService.config.private.frontServices.server + this.configService.config.private.frontServices.document.getSCORES;

  }
  /**
   * Método post para obtener estatus del token
   * @param body parametros token
   * @returns  response ...
   */
  getEstatusExpediente = (body): Observable<any> => {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
        'Access-Control-Allow-Credentials': 'true'
      }),
      observe: 'response' as 'response'
    };
    return this.httpClient.post(`${this.urlGetExpediente}`,  JSON.stringify(body), httpOptions)
      .pipe(map(res => res));
  }
  /**
   * Método Get para todas las solicitudes del estatus
   * @param body parametros token
   * @param params description
   * @param llave description
   * @returns  Respuesta del servicio
   */
  getCheckToken = (body, params, llave): Observable<any> => {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
        'Access-Control-Allow-Credentials': 'true'
      }),
      observe: 'response' as 'response'
    };
    return this.httpClient.post(`${this.urlCheckToken}?${llave}=${params[llave]}`, JSON.stringify(body), httpOptions)
      .pipe(map(res => res));
  }

  /**
   * Método Get para todas las solicitudes del access token
   * @returns  Respuesta del servicio
   */
  getToken = (): Observable<any> => {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
        'Access-Control-Allow-Credentials': 'true'
      }),
      observe: 'response' as 'response'
    };
    return this.httpClient.get(`${this.urlGetToken}`, httpOptions)
      .pipe(map(res => res));
  }

  getIdValidas = (idExpediente,idSesion): Observable<any> => {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
        'Access-Control-Allow-Credentials': 'true'
      }),
      observe: 'response' as 'response'
    };
    return this.httpClient.post(this.urlGetIdValidas+"?idExpediente="+idExpediente+"&idSesion="+idSesion,JSON.stringify({idExpediente:idExpediente,idSesion:idSesion}),httpOptions)
      .pipe(map(res => res));
  }


  getOCR = (idTransaccion, body): Observable<any> => {
    const ruta = this.urlGetOCR+'/?idTransaccion=' + idTransaccion;
    return this.httpClient.post(ruta, body)
      .pipe(map(res => res));
  }

  getSCORES = (idTransaccion, body): Observable<any> => {
    const ruta = this.urlGetSCORES+'/?idTransaccion=' + idTransaccion;
    return this.httpClient.post(ruta, body)
      .pipe(map(res => res));
  }
  
}
