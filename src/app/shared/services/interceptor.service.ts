import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
/**
 * Http interceptor for manipulating all http requests
 */
export class InterceptorService implements HttpInterceptor {
    constructor(
        private injector: Injector,
        private _http: HttpClient
    ) { }
    /**
     * Set auth token headers for each request that need it
     * @param request
     * @param next
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            withCredentials: true
        });
        return next.handle(request);
    }


}
