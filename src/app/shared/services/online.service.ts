import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';

@Injectable()
/**
 * Clase OnLineService para suscribirse al desconectarse del servidor
 */
export class OnLineService {
    /**
     * variable para almacenar modelo FilterModel
     */
    public modelSpinner: any;
    /**
     * Variable para crear observador y siscribirse a la funcion
     */
    public $subject = new BehaviorSubject<any>(this.modelSpinner);
    /**
     * Variable para guardar la informacion emitida por el observable
     */
    public spinner = this.$subject.asObservable();
    /**
     * Metodo que emite la subcripcion a los datos de las solicitudes
     */
    sendOnLine(iFilter: any) {
        this.$subject.next(iFilter);
    }
    /**
     * Metodo retorna valor mas actual a los datos de las solicitudes
     */
    getOnLine(): Observable<any> {
        return this.spinner;
    }
}
