import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';

@Injectable()
/**
 * Clase CacheService para suscribirse al modal de mensajes
 */
export class CacheService {
    /**
     * variable para almacenar modelCache
     */
    public modelCache: any;
    /**
     * Variable para crear observador y subscribirse a la funcion
     */
    public $subject = new BehaviorSubject<any>(this.modelCache);
    /**
     * Variable para guardar la informacion emitida por el observable
     */
    public modal = this.$subject.asObservable();
    /**
     * Metodo que emite la subcripcion a los datos del modal
     */
    sendCache(iModal: any) {
        this.$subject.next(iModal);
    }
    /**
     * Metodo retorna valor mas actual a los datos del modal
     */
    getCache(): Observable<any> {
        return this.modal;
    }
}
