import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';

@Injectable()
/**
 * Class statusService  for sort table transfer
 */
export class DatosService {
    /**
     * Variable for suscribe Subject
     */
    public modelStep: any;

    public $subject = new BehaviorSubject(this.modelStep);
    /**
     * Variable observable status
     */
    public status = this.$subject.asObservable();
    /**
     * Method sendstatus observator
     * @param iStatus - word to find in table transfer
     */
    sendDatos(iStatus: any): void {
        this.$subject.next(iStatus);
    }
    /**
     * Method getstatus return observable
     */
    getDatos(): Observable<any> {
        return this.status;
    }
}
