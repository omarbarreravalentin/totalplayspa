import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';

@Injectable()
/**
 * Clase StepsService para suscribirse al paso actual
 */
export class StepsService {
    /**
     * variable para almacenar modelo FilterModel
     */
    public modelStep: any;
    /**
     * Variable para crear observador y siscribirse a la funcion
     */
    public $subject = new BehaviorSubject<any>(this.modelStep);
    /**
     * Variable para guardar la informacion emitida por el observable
     */
    public step = this.$subject.asObservable();
    /**
     * Metodo que emite la subcripcion a los datos de las solicitudes
     */
    sendStep(iFilter: any) {
        this.$subject.next(iFilter);
    }
    /**
     * Metodo retorna valor mas actual a los datos de las solicitudes
     */
    getStep(): Observable<any> {
        return this.step;
    }
}
