import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';

@Injectable()
/**
 * Class CsrfService for get CSRF TOKEN
 */
export class CsrfService {
    /**
     * variable para almacenar modelo modelCSRF
     */
    public modelCSRF: any;
    /**
     * Variable for suscribe Subject
     */
    public $subject = new BehaviorSubject(this.modelCSRF);
    /**
     * Variable observable status
     */
    public status = this.$subject.asObservable();
    /**
     * Method sendstatus observator
     * @param iStatus - word to find in table transfer
     */
    sendStatus(iStatus: any): void {
        this.$subject.next(iStatus);
    }
    /**
     * Method getstatus return observable
     */
    getStatus(): Observable<any> {
        return this.status;
    }
}
