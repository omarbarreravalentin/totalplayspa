import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Enviroment service
 */
@Injectable()
export class ConfigService {
  /**
   * variable donde se guardara parametros settings provenientes de los yamels
   */
  public config: any;
  /**
   * Inicia servicio http client
   */
  constructor(private httpClient: HttpClient) { }

  /**
   * Método Get obtiene API_BASE_URL datos del yamel configurado en pass
   * @param url - url padre proyecto
   * @returns {JSON} results - variables url *
   */
  getConfig(url?: string): Promise<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'kQshhRdAgsdSCLRpkAe9',
        'Content-Type': 'text/html',
        'Accept': 'text/html',
      })
    };
    // Sirve para traer archivo json setting desde otros  server
    const localHostname = this.getParams('localHostname');
    const localPath = this.getParams('localPath');
    /*
    let location = (window.location.hostname === 'localhost')
      ? '/assets/config/settings.json'
      : (window.location.hostname === localHostname)
        ? `${localPath}/assets/config/settings.json`
        : '/backend';
    if (window.location.hostname.match(/^\d/)) {
      location = `${localPath}/assets/config/settings.json`;
    }*/
    let location="https://demobioonboarding.z13.web.core.windows.net/assets/config/settings.json";
    //let location="https://ocr-minsait.web.app/assets/config/settings.json";
    const promise = this.httpClient.get<any>
      (location, httpOptions)
      .toPromise()
      .then(settings => {
        const configura = (settings) ? settings : '';
        Object.keys(configura.public).forEach(key => {
          if (typeof configura.public[key] === 'string') {
            const txt = document.createElement('textarea');
            txt.innerHTML = configura.public[key];
            configura['public'][key] = txt.value;
          }
        });
        this.config = configura;
        return this.config;
      });
    return promise;
  }
  /**
   * Metodo que decodeURIComponent el dato
   * @param  name - Nombre parametro a decodificar de la url.*
   * @returns  results - Valor del parametro devuelto *
   */
  getParams(name: string): string {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    const regeName = `[\\?&]${name}=([^&#]*)`;
    const regex = new RegExp(regeName);
    const results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  }

  /**
   * Metodo para mostrar pantalla completa
   * @param elemento a expandir
   * @param orientation tipo de orientacion
   * @returns boolean true= si esta en pantalla completa
  */
  getFullscreen(element: any, orientation: any): boolean {
    if (document.fullscreenEnabled && !element.fullscreenElement) {
      if (element.requestFullscreen) {
        element.requestFullscreen().then(function () { }).catch(function (error) { });
      } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen().then(function () { }).catch(function (error) { });
      } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen().then(function () { }).catch(function (error) { });
      } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen().then(function () { }).catch(function (error) { });
      }
      const documentObject: any = document;
      const full_screen_element = documentObject.fullscreenElement;
      if (full_screen_element !== null) {
        switch (orientation) {
          case "unlock":
            screen.orientation.unlock();
            break;
          case undefined:
            break;
          default:
            screen.orientation.lock(orientation);
            break;
        }
        window.scroll({
          top: 100,
          behavior: 'smooth'
        });
        return true;
      } else {
        window.scroll({
          top: 0,
          behavior: 'smooth'
        });
        return false;
      }
    }
  }
}
