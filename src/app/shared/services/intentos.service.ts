import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';
import { DebugService } from './debug.service';

@Injectable()
/**
 * Clase IntentosService para suscribirse a los intentos realizados, en caso de error
 */
export class IntentosService {
    /**
     * Inicializa servicio  RedirectService
    */
    constructor(private debugService: DebugService) { }

    /**
     * variable para almacenar modelo modelSpinner
     */
    public modelSpinner: any;
    /**
     * variable para almacenar numero de intentos faltantes
     */
    private intentosFaltantes = 0;
    /**
     * variable para almacenar numero de intentos realizados
     */
    private intentosRealizados = 0;
    /**
     * Variable para crear observador y siscribirse a la funcion behavier subject
     */
    public $subject = new BehaviorSubject<any>(this.modelSpinner);
    /**
     * Variable para guardar la informacion emitida por el observable
     */
    public spinner = this.$subject.asObservable();
    /**
     * Metodo que emite la subcripcion a los datos de intentos
     */
    sendIntentos(iFilter: any) {
        if (iFilter === true) {
            this.intentosRealizados = ++this.intentosRealizados;
        } else {
            this.intentosRealizados = 0;
        }
        this.debugService.debug({
            console: true,
            msjConsole: "INFO " + new Date() + " INTENTOS_REALIZADOS " + this.intentosRealizados,
            alert: true,
            msjAlert: "INTENTOS_REALIZADOS " + this.intentosRealizados
        });
        const intentosPermitidos = 3;
        this.intentosFaltantes = intentosPermitidos - this.intentosRealizados;
        this.$subject.next({ intentos: this.intentosFaltantes });
    }
    /**
     * Metodo retorna valor mas actual de los intentos de un servicio
     */
    getIntentos(): Observable<any> {
        return this.spinner;
    }
}
