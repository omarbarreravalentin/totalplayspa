import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { ToastrService } from 'ngx-toastr';

/**
 * Clase que permite mostrar la traza contextual del proceso actual
 */
@Injectable()
export class DebugService {
  /**
   * variable donde se guardara parametros settings provenientes de los yamels
   */
  public config: any;

  /**
   * Inicialiazion del servicio ConfigService
   */
  constructor(private configService: ConfigService, private toastr: ToastrService) {
    this.config = this.configService.config.public;
  }

  /**
   * Metodo que permite desplegar la traza contextual
   * @param  msj - Mensaje a desplejar
   */
  debug(msj: object): void {
    const displayConsole = this.config.debug_console || false;
    const displayAlert = this.config.debug_alerts || false;
    if (displayConsole && msj["console"]) {
      //console.info(msj["msjConsole"]);
    }
    if (displayAlert && msj["alert"]) {
      this.toastr.success('', msj["msjAlert"]);
    }
  }
}
