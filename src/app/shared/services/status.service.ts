import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';

@Injectable()
/**
 * Class statusService  for sort table transfer
 */
export class StatusService {
    /**
     * Variable for suscribe Subject
     */
    public $subject = new BehaviorSubject(null);
    /**
     * Variable observable status
     */
    public status = this.$subject.asObservable();
    /**
     * Method sendstatus observator
     * @param iStatus - word to find in table transfer
     */
    sendStatus(iStatus: any): void {
        this.$subject.next(iStatus);
    }
    /**
     * Method getstatus return observable
     */
    getStatus(): Observable<any> {
        return this.status;
    }
}
