import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { map } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { RedirectService } from './redirect.service';
import { DatosService } from './datos.service';

@Injectable()
/**
 * Clase que provee los servicios http para el componente
 */
export class CapturaDocumentoBioService {
  /**
   * Almacena la url base proveniente del Yamel para consumir los servicios de ofertas.
   */
  private url: string;

  /**
   * Inicialiazion del servicio http asi como la obtencion de las url base
   */
  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService,
    private datosService: DatosService
  ) {
    this.url = this.configService.config.private.frontServices.server + this.configService.config.private.frontServices.chunks.chunksJoin;
  }
  /**
   * Método post para enviar documento capturado veridas
   * @param body parametros token
   * @returns  Respuesta del servicio
   */
  postChuckDocument = (body): Observable<any> => {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
        'Access-Control-Allow-Credentials': 'true'
      }),
      observe: 'response' as 'response'
    };
    return this.httpClient.post(`${this.url}`, JSON.stringify(body), httpOptions)
      .pipe(map(res => res));
  }

  /**
   * Método post para enviar documento completo capturado x veridas
   * @param body parametros token
   * @returns  Respuesta del servicio
   */
  postCompleteDocument = (type,body,idExpediente,idSesion): Observable<any> => {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
        'Access-Control-Allow-Credentials': 'true'
      }),
      observe: 'response' as 'response'
    };
    return this.httpClient.post(this.configService.config.private.frontServices.server + "/addOneFile/?type="+type+"&idExpediente="+idExpediente+"&idSesion="+idSesion, body)
      .pipe(map(res => res));
  }
}
