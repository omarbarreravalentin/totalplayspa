import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';

@Injectable()
/**
 * Clase SpinnerService para suscribirse al modal de mensajes
 */
export class ModalService {
    /**
     * variable para almacenar modelo FilterModel
     */
    public modelModal: any;
    /**
     * Variable para crear observador y siscribirse a la funcion
     */
    public $subject = new BehaviorSubject<any>(this.modelModal);
    /**
     * Variable para guardar la informacion emitida por el observable
     */
    public modal = this.$subject.asObservable();
    /**
     * Metodo que emite la subcripcion a los datos del modal
     */
    sendModal(iModal: any) {
        this.$subject.next(iModal);
    }
    /**
     * Metodo retorna valor mas actual a los datos del modal
     */
    getModal(): Observable<any> {
        return this.modal;
    }
}
