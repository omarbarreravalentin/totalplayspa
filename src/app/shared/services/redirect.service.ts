import { Compiler, Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/';
import { CsrfService } from './csrf.service';
import { DatosService } from './datos.service';

import { StepsService } from './steps.service';
import { DebugService } from './debug.service';

let ruta: string;
@Injectable()
/**
 * Class RedirectService ruta para redireccionar, en caso de error o exito
 */
export class RedirectService {
    /**
   * Inicializa servicio  RedirectService
   */
    constructor(private datosService:DatosService,private compiler: Compiler, private csrfService: CsrfService, private step: StepsService, private debugService: DebugService) { }
    /**
         * variable para almacenar modelo modelRedirect
         */
    public modelRedirect: any;
    /**
     * Variable for suscribe Subject
     */
    public $subject = new BehaviorSubject(this.modelRedirect);
    /**
     * Variable observable status
     */
    public status = this.$subject.asObservable();
    /**
     * Method sendstatus observator
     * @param iStatus - word to find in table transfer
     */
    sendStatus(iStatus: any): void {
        if (iStatus.type == 'init') {
            ruta = iStatus.url;
            this.$subject.next({ ruta: ruta });
        } else if (iStatus.code > 205) {
            this.step.sendStep("final");
            this.debugService.debug({
                console: true,
                msjConsole: "INFO " + new Date() + " REDIRECCIONANDO CON ESTATUS -1",
                alert: true,
                msjAlert: "REDIRECCIONANDO CON ESTATUS -1"
            });
            localStorage.clear();
            this.compiler.clearCache();
            this.csrfService.sendStatus(undefined);
            window.location.href = ruta + '/?statusCode=-1';
        } else if (iStatus.type == 'final' && iStatus.code < 205) {
            this.step.sendStep("final");
            this.debugService.debug({
                console: true,
                msjConsole: "INFO " + new Date() + " REDIRECCIONANDO CON ESTATUS 1",
                alert: true,
                msjAlert: "REDIRECCIONANDO CON ESTATUS -1"
            });
            localStorage.clear();
            this.compiler.clearCache();
            window.location.href = ruta + '/?statusCode=1';
        } else {
            this.$subject.next({ ruta: ruta });
        }
    }
    /**
     * Method getstatus return observable
     */
    getStatus(): Observable<any> {
        return this.status;
    }
}
