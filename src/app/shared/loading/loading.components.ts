import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'sl-loading',
  templateUrl: 'loading.html',
  styleUrls: ['../../../assets/styles/loading.css'],
})
/**
 * Clase para mostrar spinner componente
 */
export class LoadingComponent  {
  /**
   * Constante guradar texto confirmacion
   */
  @Input() txtSpinner: string;
  @Input() subtxtSpinner: string;  
  }
