import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { SpinnerService } from './shared/services/spinner.service';
import { OnLineService } from './shared/services/online.service';
import { ModalService } from './shared/services/modal.service';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ConnectionService } from 'ng-connection-service';
import { ConfigService } from './shared/services/config.service';
import { ModalComponent } from './shared/modal/modal.components';
import { ActivatedRoute, Router } from '@angular/router';
import { RedirectService } from './shared/services/redirect.service';
import { DatosService } from './shared/services/datos.service';
 
import { CsrfService } from './shared/services/csrf.service';

import { BioService } from './shared/services/bio.service';
import { DebugService } from './shared/services/debug.service';
import { StepsService } from './shared/services/steps.service';
import { timeout } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
/**
 * Clase principal de inicio donde se valida el status del documento
 */
export class AppComponent implements OnInit, OnDestroy {
  /**
   * Variable para control el status del spinner true|false
   */
  public showSpinner: boolean;
  /**
   * Variable Subscription para guardar observable spinner
   */
  public subscriptionSpinner$: Subscription;
  /**
   * Variable Subscription para guardar observable online
   */
  public subscriptionOnline$: Subscription;
  /**
   * Variable Subscription para guardar observable csrf
   */
  public subscriptionCsrf$: Subscription;
  /**
   * Variable Subscription para guardar observable connection
   */
  public subscriptionConnection$: Subscription;
  /**
   * Variable Subscription para guardar observable expediente
   */
  public subscriptionExpediente$: Subscription;
  /**
   * Variable Subscription para guardar observable router
   */
  public subscriptionRouter$: Subscription;
  /**
   * Variable boolean para validar si el aplicativo esta conectado a internet
   */
  public isConnected = true;
  /**
   * Variable para validar status de conexion del aplicativo
   */
  public status = 'ONLINE';
  /**
   * Variable para subscribirse modal
   */
  public subscriptionModal$: Subscription;
  /**
   * Variable para subscribirse validaToken
   */
  public subscriptionValidaToken$: Subscription;
  /**
   * Variable para subscribirse getToken
   */
  public subscriptionGetToken$: Subscription;
  /**
   * Variable para guardar informacion del spinner
   */
  public txtSpinner: string;
  public idExpediente: string;
  public idSesion: string;
  /**
   * Variable para guardar informacion del spinner
   */
  public subtxtSpinner: string;
  /**
   * Instancia servicios, y llama a servicio para revisar el status del expediente
   */
  constructor(
    private bioService: BioService,
    private router: Router,
    private spinnerService: SpinnerService,
    private onlineService: OnLineService,
    private connectionService: ConnectionService,
    private configService: ConfigService,
    private modalService: NgbModal,
    private _modalService: ModalService,
    private cdRef: ChangeDetectorRef,
    config: NgbModalConfig,
    private activateRouter: ActivatedRoute,
    private redirectService: RedirectService,
    private datosService:DatosService,
    private debugService: DebugService,
    private step: StepsService,
    private deviceService: DeviceDetectorService,
    private csrfService: CsrfService) {
    this.showSpinner = false;
    config.backdrop = 'static';
    config.keyboard = false;
    this.subscriptionConnection$ = this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.debugService.debug({
          console: true,
          alert: true,
          msjConsole: "ERR " + new Date() + " ONLINE_DEVICE",
          msjAlert: "ONLINE_DEVICE"
        });
        // this.subscriptionCsrf$ = this.csrfService.getStatus().subscribe(datacsrf => {
        //   if (datacsrf && datacsrf.token) {
        //     const codecsrf = datacsrf.token;
        //     const obj = {
        //       _csrf: codecsrf
        //     };
        //     this.subscriptionExpediente$ = this.bioService.getEstatusExpediente(obj).subscribe(data => {
        //       const res = JSON.parse(JSON.stringify(data));
        //       if (res.estadoAnverso === 'PENDIENTE') {
        //         this.spinnerService.sendSpinner({ status: false });
        //         this.router.navigate(['/bio/captureDocuments'], { skipLocationChange: true });
        //       }
        //       if (res.estadoAnverso === 'GUARDADO') {
        //         this.spinnerService.sendSpinner({ status: false });
        //         if (res.estadoSelfie === 'PENDIENTE') {
        //           this.router.navigate(['/bio/captureFoto'], { skipLocationChange: true });
        //         }
        //         if (res.estadoVideo === 'PENDIENTE') {
        //           this.router.navigate(['/bio/captureVideo'], { skipLocationChange: true });
        //         }
        //       }
        //     }, error => {
        //       this.spinnerService.sendSpinner({ status: false });
        //       this.router.navigate(['/'], { skipLocationChange: true });
        //     });
        this.status = 'ONLINE';
        //   }
        // });
      } else {
        this.debugService.debug({
          console: true,
          alert: true,
          msjConsole: "ERR " + new Date() + " OFFLINE_DEVICE",
          msjAlert: "OFFLINE_DEVICE"
        });
        this.status = 'OFFLINE';
        this.router.navigate(['/bio/offLine'], { skipLocationChange: true });
      }
    });
  }
  /**
   * Metodo que se suscribe a los observables spinner, online y modal asi mismo
   * llama al servicio de check del expediente
   */
  ngOnInit(): void {
    this.subscribeSpinner();
    this.subscribeOnline();
    this.subscribeModal();
    this.checkExpediente();
  }
  /**
   * Metodo obtner llave de query param y llamar al servicio de revision del token 
   */
  checkExpediente(): void {
    this.spinnerService.sendSpinner({ status: true });
    const llave = this.configService.config.public.keyQuery;
    /*this.subscriptionRouter$ = this.activateRouter.queryParams.subscribe(params => {
      if (params && params[llave]) {
        const token = params[llave].replace(/ /g, '+');
        if (this.validBrowser()) {
          if (this.validTokenLength(token)) {*/
            let ddate = new Date();
            var year = ddate.getFullYear().toString();
            var month = (ddate.getMonth() + 101).toString().substring(1);
            var day = (ddate.getDate() + 100).toString().substring(1);
            var hour = (ddate.getHours() + 100).toString().substring(1);
            var minutes = (ddate.getMinutes() + 100).toString().substring(1);
            var seconds = (ddate.getSeconds() + 100).toString().substring(1);
            var milseconds = (ddate.getMilliseconds() + 100).toString().substring(1);
            let token=day+month+year+"_"+hour+minutes+seconds+milseconds;
            this.subscriptionGetToken$ = this.bioService.getToken()
              .pipe(timeout(this.configService.config.private.frontServices.token.timeOut))
              .subscribe((response) => {
                const xsrfc = response.body;
                if (xsrfc && xsrfc["XSRF-TOKEN"]) {
                  const csrf = xsrfc["XSRF-TOKEN"];
                  const body = {
                    token: decodeURIComponent(token),
                    _csrf: csrf
                  };
                  this.csrfService.sendStatus({ token: csrf });
                  this.subscriptionValidaToken$ = this.bioService.getCheckToken(body, {token:token}, llave)
                    .pipe(timeout(this.configService.config.private.frontServices.token.timeOut))
                    .subscribe((responsex) => {
                      this.debugService.debug({
                        console: true,
                        alert: true,
                        msjConsole: "INFO " + new Date() + " VALIDANDO_TOKEN " + JSON.stringify(responsex),
                        msjAlert: " VALIDANDO_TOKEN " + responsex.status
                      });
                      const theBody = responsex.body;
                      if (theBody && theBody.redirectURL) {
                        let idExpedientexxx=theBody.idExpediente;
                        let idSesionxxx=theBody.idSesion;
                        
                        localStorage.setItem('idExpediente', idExpedientexxx);
                        localStorage.setItem('idSesion', idSesionxxx);


                        this.datosService.sendDatos({idExpediente:idExpedientexxx,idSesion:idSesionxxx});
                        this.redirectService.sendStatus({url: theBody.redirectURL,code: 0,type: 'init'});
                        if (responsex.status < 205) {
                          this.step.sendStep("tokenOK");
                          this.spinnerService.sendSpinner({ status: false });
                          this.router.navigate(['/bio']);
                        }
                      }
                    }, responsey => {
                      const code = responsey.status || 400;
                      this.debugService.debug({
                        console: true,
                        alert: true,
                        msjConsole: "INFO " + new Date() + " ESTATUS_VALIDANDO_TOKEN " + JSON.stringify(responsey),
                        msjAlert: " ESTATUS_VALIDANDO_TOKEN " + code
                      });
                      if (code === 200) {
                        this.step.sendStep("tokenOK");
                        this.spinnerService.sendSpinner({ status: false });
                        this.router.navigate(['/bio']);
                      }
                    });
                }
              });
          /*} else {
            this.debugService.debug({
              console: true,
              alert: true,
              msjConsole: "INFO " + new Date() + " INVALID_TOKEN_LENGTH ",
              msjAlert: " INVALID_TOKEN_LENGTH "
            });
          }
        } else {
          this.debugService.debug({
            console: true,
            alert: true,
            msjConsole: "INFO " + new Date() + " BROWSER_INCOMPATIBLE ",
            msjAlert: " BROWSER_INCOMPATIBLE "
          });
          this.router.navigate(['/bio/incompatible']);
        }
      }
    });*/
  }

  /**
   * Inicializacion subscripcion a spinner
   */
  subscribeSpinner(): void {
    this.subscriptionSpinner$ = this.spinnerService.getSpinner()
      .subscribe(spinner => {
        if (spinner) {
          this.showSpinner = spinner.status;
          this.cdRef.detectChanges();
          this.txtSpinner = (spinner.txtSpinner) ? spinner.txtSpinner : '';
          const subtxt = this.configService.config.public.debug_porcent_upload || false;
          if (subtxt) {
            this.subtxtSpinner = (spinner.subtxtSpinner) ? spinner.subtxtSpinner : '';
          }
        }
      });
  }

  /**
   * Inicializacion subscripcion, estado de la conexión con el servidor
   */
  subscribeOnline(): void {
    this.subscriptionOnline$ = this.onlineService.getOnLine()
      .subscribe(status => {
        if (status && status.connected === false) {
          this.router.navigate(['/bio/offLine'], { skipLocationChange: true });
        }
      });
  }

  /**
   * Inicializacion subscripcion, apertura modal
   */
  subscribeModal(): void {
    this.subscriptionModal$ = this._modalService.getModal()
      .subscribe(modal => {
        if (modal && modal.open === true) {
          this.modalService.open(ModalComponent);
        }
      });
  }

  /**
   * Funcion que valida si el browser del usuario esta soportado
   */
  validBrowser(): boolean {
    let result = true;
    try {
      const deviceInfo = this.deviceService.getDeviceInfo();
      const userBrowser = "min_version_" + deviceInfo.browser;
      const userVersionBrowser = deviceInfo.browser_version;
      const userOS = deviceInfo.os;
      if (deviceInfo.browser === 'IE' || (userOS === 'iOS' && deviceInfo.browser === 'Chrome' && this.deviceService.isMobile())) {
        result = false;
      }else if (this.configService.config.public[userBrowser] != undefined
        && parseInt(this.configService.config.public[userBrowser])>=parseInt(userVersionBrowser)) {
        result = false;
      }
    } catch (error) {
    }
    return result;
  }

  /**
   * Funcion que valida si la longitud del token opaco es valida
   */
  validTokenLength(token:string): boolean {
    let result = false;
    if(this.configService.config.public.token_minLength <= decodeURIComponent(token).length &&
    decodeURIComponent(token).length <= this.configService.config.public.token_maxLength){
      result=true;
    }
    return result;
  }

  /**
   * Metodo para ejecutar unsubscribe del componente, y limpiar intervals
   */
  ngOnDestroy() {
    try {
      this.subscriptionSpinner$.unsubscribe();
      this.subscriptionOnline$.unsubscribe();
      this.subscriptionModal$.unsubscribe();
      this.subscriptionCsrf$.unsubscribe();
      this.subscriptionConnection$.unsubscribe();
      this.subscriptionExpediente$.unsubscribe();
      this.subscriptionRouter$.unsubscribe();
      this.subscriptionValidaToken$.unsubscribe();
      this.subscriptionGetToken$.unsubscribe();
    } catch (error) {

    }
  }
}
