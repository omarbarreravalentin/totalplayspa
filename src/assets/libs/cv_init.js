/* Cargando el WebAssembly del sdk document */
var loaded=false;
try {
    var Module = {
        wasmBinaryFile: '../assets/libs/cv.wasm',
        _main: function () {
            loaded=true;
        }
    }
} catch (e) {}

/**
 *Funcion que retorna el estado del wasm
 @returns boolean, true si el wasm ya ha sido cargado
 */
function loadWASM() {
    return loaded;
}